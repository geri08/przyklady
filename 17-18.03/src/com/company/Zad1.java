package com.company;
/*
*ZADANIE #1*
Utwórz metodę, która przyjmuje listę (`List`) np. `ArrayList` elementów
typu `String` a następnie zwraca listę w odwróconej kolejności.
 */

import java.util.ArrayList;
import java.util.List;

public class Zad1 {

    public static void main(String[] args) {
        List<String> mojaLista = new ArrayList<String>();
        mojaLista.add("pierwszy"+ "dziesiaty");
        mojaLista.add("drugi");
        mojaLista.add("trzeci");
        mojaLista.add("czwarty");
        mojaLista.add("piaty");
        mojaLista.add("szosty");
        wyswietlliste(mojaLista);
        odwrocListe(mojaLista);
        System.out.println();
//        System.out.println(mojaLista);
        wyswietlliste(mojaLista);

    }

    private static void wyswietlliste(List<String> mojaLista) {
        for (int i = 0; i < mojaLista.size(); i++) {
            System.out.print(mojaLista.get(i) + ", ");
        }
    }

    private static void odwrocListe(List<String> mojaLista) {

        for (int pozycja = 0; pozycja < mojaLista.size() / 2; pozycja++) {
            int index = (mojaLista.size() - 1 - pozycja);
            String tymczasowa = mojaLista.get(pozycja);
            mojaLista.set(pozycja, mojaLista.get(index));
            mojaLista.set(index, tymczasowa);
        }
    }
}
