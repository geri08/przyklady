package com.company;
/*
Utwórz metodę, która przyjmuje dwa parametry - listę liczb (np. ArrayList)
 oraz indeks za którym należy "przeciąć" tablicę i zwrócić drugą część.

Dla 3 oraz <1, 7, 8, 22, 10, -2, 33>
powinno zwrócić <10, -2, 33>
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Zad8 {
    public static void main(String[] args) {
        int ciecie = 3;
        List<Integer> mojaLista = uzupelnijListe(10);
        System.out.println(mojaLista);
        System.out.println();
        List<Integer> ucietaLista = przytnijListe(mojaLista, ciecie);
        System.out.println(ucietaLista);
        System.out.println();

        System.out.println(szybciej(mojaLista, ciecie));
    }

    private static List<Integer> szybciej(List<Integer> mojaLista, int ciecie) {
        return mojaLista.subList(ciecie + 1, mojaLista.size());
    }

    private static List<Integer> przytnijListe(List<Integer> mojaLista, int ciecie) {
        List<Integer> ucieta = new ArrayList<>();
        for (int i = ciecie + 1; i < mojaLista.size(); i++) {
            ucieta.add(mojaLista.get(i));
        }
        return ucieta;
    }

    private static List<Integer> uzupelnijListe(int rozmiar) {
        Random r = new Random();
        List<Integer> uzupelniona = new ArrayList<>();
        for (int i = 0; i < rozmiar; i++) {
            uzupelniona.add(r.nextInt(20));
        }
        return uzupelniona;
    }
}
