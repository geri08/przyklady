package com.company;
/*
*ZADANIE #9*
Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania wartości `-1`
a następnie zwraca mapę wartości, gdzie kluczem jest liczba a wartością jest liczba jej wystąpień. (edited)
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Zad9 {
    public static void main(String[] args) {
        Map<Integer, Integer> mojaMapa = wypelnijMape();
        System.out.println(mojaMapa);

    }

    private static Map<Integer, Integer> wypelnijMape() {
        Scanner scanner = new Scanner(System.in);
        Map<Integer, Integer> mapaLiczb = new HashMap<>();
        while (true) {
            System.out.print("Podaj liczbę; ");
            int podanaLiczba = scanner.nextInt();
            if (podanaLiczba == -1) {
                break;
            }
            if (mapaLiczb.containsKey(podanaLiczba)) {
                int staraWartosc = mapaLiczb.get(podanaLiczba);
                mapaLiczb.put(podanaLiczba, ++staraWartosc);
            } else {
                mapaLiczb.put(podanaLiczba, 1);
            }
        }
        return mapaLiczb;
    }
}
