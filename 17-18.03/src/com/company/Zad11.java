package com.company;

import java.util.HashMap;
import java.util.Map;

/*
*ZADANIE #11*
Utwórz metodę, która przyjmuje dwa parametry - mapę (w postaci `klucz - wartość` np. `String - int`)
oraz poszukiwaną liczbę. Metoda ma zwrócić informację ile razy w mapie występuje (jako wartość) podana liczba.
 */
public class Zad11 {
    public static void main(String[] args) {
        Map<String, Integer> mojaMapa = new HashMap<>();

        wypelnijMape(mojaMapa);
        int ileRazy = ileRazyWystepuje(mojaMapa, 21);
        System.out.println(ileRazy);
    }

    private static void wypelnijMape(Map<String, Integer> mojaMapa) {
        mojaMapa.put("ala", 20);
        mojaMapa.put("ola", 21);
        mojaMapa.put("ela", 19);
        mojaMapa.put("cala", 20);
        mojaMapa.put("gala", 22);
        mojaMapa.put("wiola", 21);
        mojaMapa.put("dala", 22);
        mojaMapa.put("wala", 20);
        mojaMapa.put("hala", 20);
        mojaMapa.put("ula", 18);
    }

    private static int ileRazyWystepuje(Map<String, Integer> mojaMapa, int szukanaWartosc) {
        int licznik = 0;
        for (Map.Entry<String, Integer> element : mojaMapa.entrySet()) {
            if (szukanaWartosc == element.getValue()) {
                licznik++;
            }
        }
        return licznik;
    }
}
