package com.company;

import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

/*
Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania
duplikatu a następnie zwraca set (np. HashSet) (z podanymi liczbami)
 */
public class Zad13 {
    public static void main(String[] args) {
        Set<Integer> bezDuplikatu = wczytujDoDuplikatu();
        System.out.println(bezDuplikatu);
    }

    private static Set<Integer> wczytujDoDuplikatu() {
        Scanner scanner = new Scanner(System.in);
        Set<Integer> nowySet = new HashSet<>();
        for (; ; ) {
            System.out.print("podaj liczbe: ");
            int a = scanner.nextInt();
            if (nowySet.add(a)) {
                nowySet.add(a);
            } else {
                break;
            }
        }
        return nowySet;
    }
}
