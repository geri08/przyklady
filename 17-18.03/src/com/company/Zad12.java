package com.company;
/*
*ZADANIE #10*
Utwórz metodę, która która przyjmuje set (np. `HashSet`) i zwraca jego rozmiar (tzn. liczbę elementów)
 */

import java.util.HashSet;
import java.util.Set;

public class Zad12 {
    public static void main(String[] args) {
        Set<Integer> nowySet = new HashSet<>();
        nowySet.add(1);
        nowySet.add(2);
        nowySet.add(1);
        nowySet.add(1);
        nowySet.add(2);
        nowySet.add(4);
        nowySet.add(5);
        nowySet.add(6);
        System.out.println(nowySet);
    }
}
