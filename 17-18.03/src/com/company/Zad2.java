package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * ZADANIE #2*
 * Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania `-1` a następnie zwraca ich listę (np. wykorzystując implementację `ArrayList`)
 * <p>
 * ZADANIE #3*
 * Utwórz metodę, która przyjmuje listę liczb, a następnie zwraca ich sumę.
 * <p>
 * ZADANIE #4*
 * Utwórz metodę, która przyjmuje listę liczb, a następnie największą z nich
 * <p>
 * ZADANIE #5*
 * Utwórz metodę, która przyjmuje listę liczb, a następnie zwraca różnicę pomiędzy największa a najmniejszą.
 */

public class Zad2 {
    public static void main(String[] args) {
        List<Integer> listaLiczb = pobierzLiczbe();
        wyswietlListe(listaLiczb);
        int sumaLiczb = sumujliste(listaLiczb);
        System.out.println("suma liczb to: " + sumaLiczb);
        int najwieksza = najwiekszaLiczba(listaLiczb);
        System.out.println("najwieksza liczba to: " + najwieksza);
        int roznica = policzRoznice(listaLiczb);
        System.out.println("roznica liczb to: " + roznica);
    }

    private static int policzRoznice(List<Integer> listaLiczb) {
        int najwieksza = listaLiczb.get(0);
        int najmniejsza = listaLiczb.get(0);

        for (Integer sprawdzanaLiczba : listaLiczb) {
            if (sprawdzanaLiczba > najwieksza) {
                najwieksza = sprawdzanaLiczba;
            }
            if (sprawdzanaLiczba < najmniejsza) {
                najmniejsza = sprawdzanaLiczba;
            }
        }
//        for (int i = 0; i < listaLiczb.size(); i++) {
//            int sprawdzanaLiczba = listaLiczb.get(i);
//            if (sprawdzanaLiczba>najwieksza){
//                najwieksza=sprawdzanaLiczba;
//            }
//            if ( sprawdzanaLiczba<najmniejsza){
//                najmniejsza = sprawdzanaLiczba;
//            }
//        }
        return najwieksza - najmniejsza;
    }

    private static int najwiekszaLiczba(List<Integer> listaLiczb) {
        int i = 1;
        int najwiekszaLiczba = listaLiczb.get(0);
        while (i < listaLiczb.size()) {
            int sprawdzanaLiczba = listaLiczb.get(i);
            if (sprawdzanaLiczba > najwiekszaLiczba) {
                najwiekszaLiczba = sprawdzanaLiczba;
            }
            i++;
        }
        return najwiekszaLiczba;
    }

    private static int sumujliste(List<Integer> listaLiczb) {
        int suma = 0;
        for (int i = 0; i < listaLiczb.size(); i++) {
            suma += listaLiczb.get(i);
        }
        return suma;
    }

    private static void wyswietlListe(List<Integer> listaLiczb) {
        System.out.print("lista liczb --> ");
        for (Integer element : listaLiczb) {
            System.out.print(element + ", ");
        }
        System.out.println("\b\b");
    }

    private static List<Integer> pobierzLiczbe() {
        List<Integer> mojaList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print(" podaj liczbę : ");
            int liczba = scanner.nextInt();
            if (liczba == -1) {
                return mojaList;
            }
            mojaList.add(liczba);
        }
    }
}
