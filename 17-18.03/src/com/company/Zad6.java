package com.company;
/*
*ZADANIE #6*
Utwórz metodę, która przyjmuje dwa parametry - listę liczb (np. `ArrayList`)
oraz poszukiwaną liczbę. Metoda ma zwrócić listę indeksów pod którymi występuje poszukiwana liczba.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Zad6 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int liczbaElementow = pobierzLiczbeElementow();
        int poszukiwanaLiczba = pobierzPoszukiwana();

        List<Integer> mojaLista = uzupelnijListe(liczbaElementow);
        List<Integer> listaIndeksow = znajdzPoszukiwaneIndeksy(mojaLista, poszukiwanaLiczba);
        String znak = ustalLiczbeZnakow(poszukiwanaLiczba);
        sprawdzCzyLiczbaSieZnajduje(listaIndeksow, mojaLista, znak);
    }

    private static void sprawdzCzyLiczbaSieZnajduje(List<Integer> listaIndeksow, List<Integer> mojaLista, String znak) {
        if (listaIndeksow.isEmpty()) {
            System.out.println("nie ma takiej liczby w tej tablicy ;( ");
        } else {
            wyswietlInaczej(mojaLista, listaIndeksow, znak);
        }
    }

    private static String ustalLiczbeZnakow(int poszukiwanaLiczba) {
        String znak = "x";
        if (poszukiwanaLiczba >= 0) {
            for (int i = 10; i <= poszukiwanaLiczba; i *= 10) {
                znak += "x";
            }
            return znak;
        } else {
            for (int i = 10; i <= (poszukiwanaLiczba * (-1)); i *= 10) {
                znak += "x";
            }
            return znak + "x";
        }
    }

    private static void wyswietlInaczej(List<Integer> mojaLista, List<Integer> listaIndeksow, String znak) {
        wyswietlListe(mojaLista);
        int drugiIndeks = 0;
        for (int indeks = 0; indeks < mojaLista.size(); indeks++) {
            if (indeks == listaIndeksow.get(drugiIndeks)) {
                System.out.print(znak + "\t");
                drugiIndeks++;
                if (drugiIndeks == listaIndeksow.size()) {
                    break;
                }
            } else {
                System.out.print("\t");
            }
        }
    }

    private static void wyswietlListe(List<Integer> mojaLista) {
        for (int indeks = 0; indeks < mojaLista.size(); indeks++) {
            System.out.print(mojaLista.get(indeks) + "\t");
        }
        System.out.println();
    }

    private static List<Integer> znajdzPoszukiwaneIndeksy(List<Integer> mojaLista, int poszukiwanaLiczba) {
        List<Integer> listaIndeksow = new ArrayList<>();
        for (int indeks = 0; indeks < mojaLista.size(); indeks++) {
            if (mojaLista.get(indeks) == poszukiwanaLiczba) {
                listaIndeksow.add(indeks);
            }
        }
        return listaIndeksow;
    }

    private static int pobierzPoszukiwana() {
        System.out.print("podaj poszukiwana liczbę: ");
        return scanner.nextInt();
    }

    private static List<Integer> uzupelnijListe(int liczbaElementow) {
        Random r = new Random();
        List<Integer> uzupelnionaLista = new ArrayList<>();
        for (int i = 0; i < liczbaElementow; i++) {
            uzupelnionaLista.add(i, r.nextInt(201) - 100);
        }
        return uzupelnionaLista;
    }

    private static int pobierzLiczbeElementow() {
        System.out.print("podaj liczbe elementow do wczytania: ");
        return scanner.nextInt();
    }
}
