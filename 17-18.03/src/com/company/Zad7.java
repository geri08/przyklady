package com.company;
/*
*ZADANIE #7*
Utwórz metodę, która przyjmuje listę liczb, a następnie zwraca listę, która
będzie zawierała dwie listy. Na pozycji `0` mają być elementy parzyste, a na pozycji `1` elementy nieparzyste.
 */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Zad7 {
    public static void main(String[] args) {
        int zakres = 10;
        List<Integer> listaRandom = wygenerujListe(zakres);
        wyswietlListe(listaRandom);
        List<List<Integer>> podzielonaLista = podzielListe(listaRandom);
        System.out.println();
        System.out.println(podzielonaLista);
    }

    private static List<List<Integer>> podzielListe(List<Integer> listaRandom) {
        List<Integer> listaParzystych = new ArrayList<>();
        List<Integer> listaNieParzystych = new ArrayList<>();
        List<List<Integer>> podzielona = new ArrayList<>();
        for (int i = 0; i < listaRandom.size(); i++) {
            int liczba = listaRandom.get(i);
            if (liczba % 2 == 0) {
                listaParzystych.add(liczba);
            } else {
                listaNieParzystych.add((liczba));
            }
        }
        podzielona.add(listaParzystych);
        podzielona.add(listaNieParzystych);
        return podzielona;
    }

    private static void wyswietlListe(List<Integer> listaRandom) {
        for (Integer element : listaRandom) {
            System.out.print(element + ", ");
        }
        System.out.print("\b\b");
    }

    private static List<Integer> wygenerujListe(int zakres) {
        Random r = new Random();
        List<Integer> wygenerowana = new LinkedList<>();
        for (int i = 0; i < zakres; i++) {
            wygenerowana.add(r.nextInt(20));
        }
        return wygenerowana;
    }
}

