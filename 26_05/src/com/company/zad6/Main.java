package com.company.zad6;

public class Main {
    public static void main(String[] args) {
        String napis = "Tekst";
        System.out.println(napis.hashCode());
        napis+=" z dodatkiem";
        System.out.println(napis.hashCode());
        String napis2 = napis;
        String napis3 = "Tekst z dodatkiem";
        System.out.println(napis2.hashCode());
        System.out.println(napis3.hashCode());
        String napis4 = "Ala ma kota";
        System.out.println(napis4.hashCode());
        String napis5 = "Ala";
        System.out.println(napis5.hashCode());
        napis5+= " ma kota";
        System.out.println(napis5.hashCode());
    }
}
