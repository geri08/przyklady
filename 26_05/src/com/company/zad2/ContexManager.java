package com.company.zad2;

public class ContexManager implements AutoCloseable {
    public ContexManager() {
        System.out.println("utworozno obiekt");
    }

    public void work() {
        System.out.println("work in progress");
    }

    @Override
    public void close() {
        System.out.println("obiekt został zamkniety");
    }
}
