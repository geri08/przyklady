package com.company.zad2;

public class Main {
    public static void main(String[] args) {
        method1();
    }

    private static void method1() {
        try(ContexManager contexManager = new ContexManager()){
            contexManager.work();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
