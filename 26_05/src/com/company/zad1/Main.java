package com.company.zad1;

import java.io.FileInputStream;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
//        metoda1();
        metoda2();
    }

    public static void metoda1() throws IOException {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream("text.txt");
            int znak = fileInputStream.read();
            while (znak != -1) {
                System.out.print((char) znak);
                znak = fileInputStream.read();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                fileInputStream.close();
            }
        }
    }

    public static void metoda2() {
        try (FileInputStream fileInputStream =
                     new FileInputStream("text.txt")) {
            int znak = fileInputStream.read();
            while (znak != -1) {
                System.out.print((char) znak);
                znak = fileInputStream.read();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
