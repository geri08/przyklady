package com.company.zad5;

import java.util.*;

public class Main {
    public static void main(String[] args) {
//        metoda1();
        metoda2();
    }

    private static void metoda2() {
        List<String> stringList = new ArrayList<>(Arrays.asList(
                "piotr", "ania", "majka", "aga"));
        ListIterator<String> listIterator = stringList.listIterator();
        while (listIterator.hasNext()) {
            String element = listIterator.next();
            System.out.println(element);
        }
        while (listIterator.hasPrevious()) {
            String element = listIterator.previous();
            System.out.println(element);
        }
    }

    private static void metoda1() {
        List<String> stringList = new ArrayList<>(Arrays.asList(
                "piotr", "ania", "majka", "aga"));
        Iterator<String> iterator = stringList.iterator();
        while (iterator.hasNext()) {
            String element = iterator.next();
            System.out.println(element);
        }

    }
}
