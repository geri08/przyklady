package com.company.zad3;

public class HtmlContextManager implements AutoCloseable {
    private String znak;

    public HtmlContextManager(String znak) {
        this.znak = znak;
        System.out.printf("<%s>", znak);
    }

    public void setText(String text) {
        System.out.printf("przekazany tekst: " + text);
    }

    @Override
    public void close() {
        System.out.printf("<\\%s>", znak);
    }
}
