package com.company.zad4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        method1();
//        method2();
        method3();
    }

    private static void method3() {
        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("adam", 21));
        studentList.add(new Student("zofia", 23));
        studentList.add(new Student("andrzej", 23));
        studentList.add(new Student("darek", 20));
        studentList.add(new Student("darek", 25));
        studentList.sort(new NameComparator());
        System.out.println(studentList);
//        studentList.sort(new AgeComparator());
//        System.out.println(studentList);
        studentList.sort(new NameAgeComparator());
        System.out.println(studentList);
    }

    private static void method2() {
        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("adam", 21));
        studentList.add(new Student("zofia", 23));
        studentList.add(new Student("kamila", 22));
        studentList.add(new Student("darek", 20));
        studentList.add(new Student("darek", 25));
        Collections.sort(studentList);
        System.out.println(studentList);
    }

    public static void method1() {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("zuzia", 20));
        personList.add(new Person("ola", 10));
        personList.add(new Person("irena", 60));
        personList.add(new Person("aga", 50));
//        Collections.sort(personList);
//        System.out.println(personList.toString());
        personList.stream()
                .sorted()
                .forEach(e -> System.out.println(e));
    }
}
