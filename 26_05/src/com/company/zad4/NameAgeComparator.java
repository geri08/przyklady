package com.company.zad4;

import java.util.Comparator;

public class NameAgeComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        if (o1 instanceof Student && o2 instanceof Student) {
            Student student1 = (Student) o1;
            Student student2 = (Student) o2;
            int result = student1.getAge() - student2.getAge();
            if(result==0){
                return student1.getName().compareTo(student2.getName());
            }
            return result;
        }
        return 0;
    }
}
