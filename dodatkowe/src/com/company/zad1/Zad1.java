package com.company.zad1;

import java.util.*;
import java.util.stream.Collectors;

/*
utwórz metodę, która przyjmuję liste liczb ( powinna zostac wczesniej wylosowana) a nastepnie zwraca mapę, która bedzie zawietała
dwa klucze(parzyste,nieparzyste bedace typami wyliczeniowymi). Na kazdej z pozycji ma być lista. Na pozycji parzyste
maja byc parzyste.
 */
public class Zad1 {
    public static void main(String[] args) {
        List<Integer> lista = stworzListe(15,20,40);
        Map<NumberTypes, List<Integer>> mapa = createMap(lista);
        System.out.println(mapa);
    }

    private static Map<NumberTypes, List<Integer>> createMap(List<Integer> lista) {
        Map<NumberTypes, List<Integer>> nowaMapa = new HashMap<>();
        List<Integer> parzyste;
        List<Integer> nieParzyste;
        parzyste = lista.stream()
                .filter(e -> e % 2 == 0)
                .collect(Collectors.toList());
        nieParzyste = lista.stream()
                .filter(e -> e % 2 != 0)
                .collect(Collectors.toList());
        nowaMapa.put(NumberTypes.PARZYSTE, parzyste);
        nowaMapa.put(NumberTypes.NIEPARZYSTE, nieParzyste);
        return nowaMapa;
    }

    private static List<Integer> stworzListe(int size) {
        return stworzListe(size, 20, 40);
    }

    private static List<Integer> stworzListe(int size, int min, int max) {
        List<Integer> nowaLista = new ArrayList<>();
        Random r = new Random();
        for (int i = 0; i < size; i++) {
            nowaLista.add(r.nextInt(max - min) + min);
        }
        return nowaLista;
    }
}
