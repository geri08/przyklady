package com.company.zad2;

import java.io.*;
import java.util.*;

/*
utwórz metodę, która przyjmuje Set obiektów klasy Samochod (  z polami nazwa oraz cena) a nastepnie zapisje
całą jego zawartosc do pliku w pewnej uporządkowanej formie (  np oddzielone przecinkami lub enterami)
Nastepnie utwórz druga metodę, która odczytuje zawartosc wczesniej zapisanego pliku i zwraca listę obiektów klasy Samochod
Program powinien byc odporny na błedne dane wejściowe i zgłaszać użytkownikowi odpowiedni komunikat.

 */
public class Main {
    public static void main(String[] args) {
        Set<Car> setOfCars = uzupelnijSeta(5);
        List<Car> listOfCars = null;
        String fileName = "car1s.t2xt";
        try {
            saveToFile(setOfCars, fileName);
        } catch (IOException e) {
            System.out.println("bład z odczytem/zapisem do pliku");
        }
        try {
            listOfCars = createList(fileName);
        } catch (IOException e) {
            System.out.println();
        }

        System.out.println(setOfCars.toString());
        System.out.println(listOfCars.toString());

    }

    private static List<Car> createList(String fileName) throws IOException {
        List<Car> carList = new ArrayList<>();
        String file = check(fileName);
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = bufferedReader.readLine();
        while (line != null) {
            String[] tablica = line.split(" ");
            carList.add(new Car(tablica[0], Integer.valueOf(tablica[1])));
            line = bufferedReader.readLine();
        }
        return carList;
    }


    private static void saveToFile(Set<Car> cars, String fileName) throws IOException {
        String file = check(fileName);
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        for (Car car : cars) {
            String line = car.zamienNaNapis();
            bufferedWriter.write(line);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();


    }

    private static String check(String fileName) {
        String match = "[0-9a-zA-Z]+.txt";
        return fileName.matches(match) ? fileName : "cars.txt";

    }

    private static Set<Car> uzupelnijSeta(int size) {
        Set<Car> cars = new HashSet<>();
        Random r = new Random();
        String[] names = new String[]{"audi", "bmv", "fiat", "skoda", "honda", "toyota"};
        for (int i = 0; i < size; i++) {
            String name = names[r.nextInt(names.length)];
            cars.add(new Car(name, r.nextInt(30000)));
        }
        return cars;
    }
}
