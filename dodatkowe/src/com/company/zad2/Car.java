package com.company.zad2;

public class Car {
    private String nazwa;
    private int cena;

    public Car(String nazwa, int cena) {
        this.nazwa = nazwa;
        setCena(cena);
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getCena() {
        return cena;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setCena(int cena) {
        if (cena > 0) {
            this.cena = cena;
        }
    }

    public String zamienNaNapis() {
        return nazwa + " " + cena;
    }

    @Override
    public String toString() {
        return "Car{" +
                "nazwa='" + nazwa + '\'' +
                ", cena=" + cena +
                '}';
    }
}
