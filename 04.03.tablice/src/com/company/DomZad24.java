package com.company;
/*
Utwórz metodę, która przyjmuje dwa dwie tablice. Metoda ma zwrócić nową
tablicę w której elementy będą wstawione na zmianę.

Dla [1, 1, 1, 1, 1]
oraz [2, 2, 2]
Ma zwrócić [1, 2, 1, 2, 1, 2, 1, 1]
 */

import java.util.Arrays;

public class DomZad24 {
    public static void main(String[] args) {
        int[] pierwsza = new int[]{1, 3, 5, 7,9,11};
        int[] druga = new int[]{2, 4, 6,8,10,};
        System.out.println(Arrays.toString(nowa(pierwsza, druga)));
    }

    private static int[] nowa(int[] pierwsza, int[] druga) {
        int zmiana[] = new int[pierwsza.length + druga.length];
        for (int i = 0, licznik = 0; i < pierwsza.length; i++) {
            zmiana[licznik] = pierwsza[i];
            licznik += 2;
        }
        for (int i = 0, licznik = 1; i < druga.length; i++) {
            zmiana[licznik] = druga[i];
            licznik += 2;
        }
        return zmiana;
    }
}

