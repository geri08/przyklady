package com.company;
/*
Utwórz metodę, która przyjmuje dwa parametry - długość i szerokość tablicy, a następnie zwraca
 nowo utworzoną, dwuwymiarową tablicę wypełnioną losowymi wartościami. Utwórz drugą metodę do wyświetlania zwróconej tablicy.
 */
import java.util.Arrays;
import java.util.Random;

public class DomZadWielo1 {
    public static void main(String[] args) {
        wyswietl(nowaTablica(6, 8));
    }

    private static void wyswietl(int[][] tablica) {
        for (int[] x : tablica)
            System.out.println(Arrays.toString(x));
    }

    private static int[][] nowaTablica(int pierwszy, int drugi) {
        Random random = new Random(20);
        int[][] tablica = new int[drugi][pierwszy];
        for (int i = 0; i < drugi; i++) {
            for (int j = 0; j < pierwszy; j++) {
                tablica[i][j] = random.nextInt(20);
            }
        }
        return tablica;
    }
}
