package com.company;

/*
Utwórz metodę, która wyświetli prostokąt o podanych wymiarach (użytkownik podaje  jego wymiary jako parametry)
XXXXXXXXXX
X        X
X        X
XXXXXXXXXX
 */
public class Zad4 {
    public static void main(String[] args) {
        String znak = "x";
        prostokat(8, 4, znak);
    }

    private static void prostokat(int wys, int szer, String znak) {
        goradol(szer, znak);
        srodek(wys, szer, znak);
        goradol(szer, znak);
    }

    private static void srodek(int wys, int szer,String znak) {
        for (int i = 0; i < wys - 2; i++) {
            System.out.print(znak);
            for (int l = 0; l <= szer - 2; l++) {
                System.out.print("  ");
            }
            System.out.print(" "+znak);
            System.out.println();
        }
    }

    private static void goradol(int szer, String znak) {
        for (int j = 0; j <= szer; j++) {
            System.out.print(znak+ " ");
        }
        System.out.println();
    }
}
