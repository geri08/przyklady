package com.company;
/*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę. Metoda ma "przesunąć"
elementy w środku tablicy o tyle ile wynosi wartość drugiego elementu i zwrócić nową tablicę.
Dla wartości dodatniej przesuwa w prawo, a dla ujemnej w lewo. Elementy z końca tablicy lądują na początku (i na odwrót)

dla ([1,2,3,4,5,6], 2)
należy zwrócić [5,6,1,2,3,4] (przesunięcie o dwa w prawo)

dla ([1,2,3,4,5,6], -3)
należy zwrócić [5,6,1,2,3,4] (przesunięcie o dwa w prawo)
 */

import java.util.Arrays;

import static java.lang.System.out;

public class DomZad26 {
    public static void main(String[] args) {
        int tablica[] = new int[]{1, 2, 3, 4, 5, 6};
        int przesuniecie = -2;
        wKtoraStrone(tablica, przesuniecie);
    }

    private static void wKtoraStrone(int[] tablica, int przesuniecie) {
        if (przesuniecie >= 0) {
            out.println(Arrays.toString(prawo(tablica, przesuniecie)));
        } else {
            out.println(Arrays.toString(lewo(tablica, przesuniecie)));
        }
    }

    private static int[] lewo(int[] tablica, int przesuniecie) {
        int tablicaWLewo[] = new int[tablica.length];
        przesuniecie *= -1;
        int granica = tablica.length - przesuniecie;
        for (int i = 0, k = 0; i < tablicaWLewo.length; i++, k++) {
            for (int j = granica; j < tablicaWLewo.length & i < przesuniecie; j++) {
                tablicaWLewo[j] = tablica[i];
                i++;
            }
            tablicaWLewo[k] = tablica[i];
        }
        return tablicaWLewo;
    }

    private static int[] prawo(int[] tablica, int przesuniecie) {
        int tablicaWPrawo[] = new int[tablica.length];
        for (int i = 0, k = 0; i < tablicaWPrawo.length; i++, k++) {
            for (int j = tablica.length - przesuniecie; j < tablica.length & i < przesuniecie; j++) {
                tablicaWPrawo[i] = tablica[j];
                i++;
            }
            tablicaWPrawo[i] = tablica[k];
        }
        return tablicaWPrawo;
    }
}
