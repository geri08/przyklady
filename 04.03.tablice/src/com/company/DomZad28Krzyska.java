package com.company;
public class DomZad28Krzyska {

    public static void main(String[] args) {

        int[] table = { 1,2,2,3,4,5,5,6} ;

        System.out.println("Długość tablicy bez duplikatów wynosi " + uniqueElements(table));

    }

    private static int uniqueElements(int[] table) {
        int placeholder = 0; // Liczba, którą zastąpię zdublowane pozycje w tablicy
        while (inTable(table, placeholder)) { // Jeżeli liczba jest w tablicy, znajdź inną
            placeholder++;
        }

        for (int i = 0; i < table.length; i++) {
            for (int j = i + 1; j < table.length; j++) {
                if ((table[i] == table[j]) && table[i] != placeholder) {
                    table[j] = placeholder; // Zastępuje wszystkie duplikaty "placeholderem"
                }
            }
        }
        int uniqueCount = 0; // Pętla zlicza wszystkie liczby poza "placeholderem"
        for (int i: table) {
            if (i != placeholder) {
                uniqueCount++;
            }
        }
        return uniqueCount;
    }

    private static boolean inTable(int[] array, int liczba) {
        for (int i : array) {
            if (liczba == i) {
                return true;
            }
        }
        return false;
    }
}