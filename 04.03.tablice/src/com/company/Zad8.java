package com.company;

/*
*ZADANIE #8*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę. Metoda ma zwrócić indeks szukanego elementu.
Gdy dana liczba nie będzie znajdować się w tablicy, metoda powinna zwrócić wartość `-1`.
 */
public class Zad8 {
    public static void main(String[] args) {
        int tablica[] = new int[]{5, 3, 2, 22, 12, 26, 7, 19};
        System.out.println(szukaj(tablica, 19));
        System.out.println(szukaj(tablica, 5));
        System.out.println(szukaj(tablica, 1));
    }

    private static int szukaj(int[] tablica, int szukana) {
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] == szukana) {
                return i;
            }
        }
        return -1;
    }
}

