package com.company;
/*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma zwrócić informację ile razy w tablicy występuje liczba podana jako drugi parametr.
 */

public class Dom16 {
    public static void main(String[] args) {
        int [] tablica = new int[]{3,4,5,2,3,2,4,5,6,3,2,3,3,3,3,2};
        int poszukiwana = 1;
        System.out.println(sprawdz(tablica,poszukiwana));
        System.out.println(35+25+24+24+39+28+29+27+26+23+35+30+24+22+26+33+27+27+26+19+18+28+25+19+44+30+31+31+37+34);
        System.out.println( 846+1627);

    }

    private static int sprawdz(int[] tablica, int poszukiwana) {
        int licznik = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] ==poszukiwana){
                licznik++;
            }
        }
        return licznik;
    }
}
