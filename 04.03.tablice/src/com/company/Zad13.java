package com.company;

import java.util.Arrays;

/**
 * Utwórz metodę, która przyjmuje dwie tablice. Metoda ma zwrócić sumę podanych tablic.
 * To znaczy element na pozycji `0` z pierwszej tablicy, dodajemy do elementu na pozycji `0`
 * z drugiej tablicy. Tablice mają być tych samych długości.
 * >`[1,  2,  3,  4]`
 * >`[5,  6,  7,  8]`
 * >`[6,  8, 10, 11]`
 */

public class Zad13 {
    public static void main(String[] args) {
        int[] pierwsza = new int[]{2, 3, 4, 6};
        int[] druga = new int[]{2, 2, 2, 2};
        System.out.println(Arrays.toString(sumujtablice(pierwsza, druga)));
    }

    private static int[] sumujtablice(int[] pierwsza, int[] druga) {
        int[] trzecia = new int[pierwsza.length];
        for (int i = 0; i < trzecia.length; i++) {
            trzecia[i] = pierwsza[i] + druga[i];
        }
        return trzecia;
    }
}
