package com.company;
/*
Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz dwie liczby. Metoda ma zwrócić sumę elementów w podanym przedziale.
Dla ([1, 2, 3, 4, 5], 2, 4)
zwróci 12, bo 3 + 4 + 5
 */

import java.util.Arrays;

public class Dom13 {
    public static void main(String[] args) {
        int tablica[] = new int[]{1,2,3,4,5};
        int pierwsza, druga;
        System.out.println(przedzial(tablica, 2, 4));
    }

    private static int przedzial(int[] tablica, int pierwsza, int druga) {
        int suma=0;
        for (int i = pierwsza; i <=druga ; i++) {
            suma+= tablica[i];
        }
        return suma;
    }
}
