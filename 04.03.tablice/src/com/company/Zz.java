package com.company;

public class Zz {
    public static void main(String[] args) {
        int szerokosc = 6;
        int wysokosc = 9;
        String znak = "x";


        narysujProstokat(szerokosc, wysokosc, znak);
    }

    private static void narysujProstokat(int szerokosc, int wysokosc, String znak) {
        int i = 0;

        goraDol(szerokosc, znak);

        while (i < wysokosc - 2){
            int j = 0;
            System.out.print(znak);
            while (j <= szerokosc - 2){
                System.out.print(" ");
                j++;
            }
            System.out.print(znak + "\n");
            i++;
        }
        goraDol(szerokosc, znak);
    }

    private static void goraDol(int szerokosc, String znak) {
        int i = 0;
        while (i <= szerokosc){
            System.out.print(znak);
            i++;
        }
        System.out.println();
    }
}

