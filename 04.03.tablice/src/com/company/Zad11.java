package com.company;

/**
 * Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz dwie liczby. Metoda ma zwrócić
 * nową tablicę do której na wybranej pozycji (drugi parametr) wstawi nowy element (trzeci parametr).
 * > Dla `([1, 2, 3, 4, 5], 2, 77)`
 * > powinno zwrócić `[1, 2, 77, 3, 4, 5]`
 */
public class Zad11 {
    public static void main(String[] args) {
        int[] tablica = new int[]{1, 5, 7, 9, 90, 98};

        wyswietltablice(tablica);
        wyswietltablice(zamiana(tablica, 3, 12));
    }

    private static void wyswietltablice(int[] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + ", ");
        }
        System.out.println();
    }

    private static int[] zamiana(int[] tablica, int index, int nowa) {
        int[] nowaTablica = new int[tablica.length + 1];
        int licznik = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (index == i) {
                nowaTablica[licznik] = nowa;
                licznik++;
            }
            nowaTablica[licznik] = tablica[i];
            licznik++;
        }
        return nowaTablica;
    }
}
