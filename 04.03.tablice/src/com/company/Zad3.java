package com.company;
/*
Utwórz metodę, która przyjmuje dwa parametry oraz *wyświetla* ciąg elementów.
Pierwszy parametr metody określa wyświetlany element, a drugi parametr liczbę wystąpień,
 */
public class Zad3 {
    public static void main(String[] args) {
        wyswietl(9, 6);
    }

    private static void wyswietl(int liczba, int razy) {
        int i = 0;
        while (i < razy) {
            int j = 0;
            while (j <= i) {
                System.out.print(liczba);
                j++;
            }
            System.out.print("_");
            i++;
        }

    }
}
//        for (int i = 0; i < razy; i++) {
//            for (int j = 0; j <= i; j++) {
//                System.out.print(liczba);
//            }
//            System.out.print(" \n");
//        }
//    }

