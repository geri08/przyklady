package com.company;
/*
Utwórz metodę, która zwraca tablicę o podanym rozmierze wypełnioną
losowymi liczbami (uzyj klasy Random()). Rozmiar tablicy ma być parametrem metody.
 */

import java.util.Arrays;
import java.util.Random;

public class DomZad3 {
    public static void main(String[] args) {
        int rozmiar =5;
        int tablica[]=new int[rozmiar];
        System.out.print(Arrays.toString(wypelnijTablice(tablica, rozmiar)));
    }

    private static int[] wypelnijTablice(int[] tablica,int rozmiar) {
        Random random = new Random();
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(100);
        }
        return tablica;
    }
}
