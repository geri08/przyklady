package com.company;
//*Utwórz metodę, która wyświetli figurę o podanych wymiarach. Metoda przyjmuje cztery parametry i wyświetli:
//
//Na przykład dla (2, 6, 8, 5)
//
//  XX
//  XX
//  XX
//  XXXXXX
//  XXXXXX
//  XXXXXX
//  XXXXXX
//  XXXXXX

public class Dom7 {
    public static void main(String[] args) {
        int gora, dol, lewybok, prawybok;
        narysuj(2, 5, 6, 8);
    }

    private static void narysuj(int gora, int dol, int lewybok, int prawybok) {
        gornyKsztalt(gora,dol,lewybok,prawybok);
        dolnyKsztalt(dol, prawybok);
    }

    private static void gornyKsztalt(int gora,int dol, int lewybok, int prawybok) {
       if(lewybok>prawybok){
           for (int j = 0; j < lewybok - prawybok; j++) {
               for (int i = 0; i < gora; i++) {
                   System.out.print("x");
               }
               System.out.println();
           }
       }
       else{
           for (int i = 0; i<prawybok-lewybok ; i++) {
               for (int j =dol-gora ; j>0 ; j--) {
                   System.out.print(" ");
               }
               for (int j = 0; j <gora; j++) {
                   System.out.print("x");
               }
               System.out.println();
           }
       }
    }

    private static void dolnyKsztalt(int dol, int prawybok) {
        for (int i = 0; i < prawybok; i++) {
            for (int j = 0; j < dol; j++) {
                System.out.print("x");
            }
            System.out.println();
        }
    }
}
