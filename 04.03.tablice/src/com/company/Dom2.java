package com.company;
/*
Utwórz metodę, w której użytkownik podaje z klawiatury
rozmiar tablicy, a następnie wszystkie elementy:
 */

import java.util.Scanner;

public class Dom2 {
    public static void main(String[] args) {

        utworzTablice();
    }

    private static void utworzTablice() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj rozmar tablicy: ");
        int rozmiar = scanner.nextInt();
        int[] tablica = new int[rozmiar];
        for (int i = 0; i < rozmiar; i++) {
            System.out.printf("podaj %s element tablicy:", (i + 1));
            tablica[i] = scanner.nextInt();
        }
        for (int i = 0; i < tablica.length; i++) {
            System.out.println(tablica[i]);
        }
    }
}
