package com.company;
/*
Utwórz metodę, która przyjmuje tablicę liczb i zwraca nową tablicę w której
wszystkie nieparzyste liczby będą w lewej części, a wszystkie parzyste będą w prawej części.
 Liczby parzyste i nieparzyste mogą być w dowolnej kolejności.
*/

import java.util.Arrays;

public class DomZad30 {
    public static void main(String[] args) {
        int tablica[] = new int[]{1, 2, 6, 4, 5, 7, 8, 3, 2};
        System.out.println(Arrays.toString(posegreguj(tablica)));
    }

    private static int[] posegreguj(int[] tablica) {
        int licznik = policzNieparzyste(tablica);
        int[] posegregowana = new int[tablica.length];
        for (int i = 0, j = 0; i < posegregowana.length; i++) {
            if (tablica[i] % 2 != 0) {
                posegregowana[j] = tablica[i];
                j++;
            } else {
                posegregowana[licznik] = tablica[i];
                licznik++;
            }
        }
        return posegregowana;
    }

    private static int policzNieparzyste(int[] tablica) {
        int licznik = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] % 2 != 0) {
                licznik++;
            }
        }
        return licznik;
    }
}
