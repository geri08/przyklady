package com.company;

import java.util.Scanner;

/*
Utwórz metodę, która przyjmuje **jeden** parametr
(który jest liczbą wierszy i kolumn) oraz wyświetla tabliczkę
 mnożenia (znaki możesz oddzielać znakiem tabulacji `\t`).
 */
public class TabliczkaMnozenia {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj liczbę do tabliczki: ");
        int a = scanner.nextInt();
        tabliczka(a);
    }

    private static void tabliczka(int a) {
        for (int i = 1; i <= a; i++) {
            for (int j = 1; j <= a; j++) {
                System.out.print(i * j + "\t");
            }
            System.out.println();
        }
    }
}
