package com.company;

/*
*ZADANIE #5 i 6*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca SUMĘ wszystkich elementów
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca ŚREDNIĄ wszystkich elementów (edited)
 */
public class Zad5 {
    public static void main(String[] args) {
        int[] tablica = new int[]{22, 3, 5, 14, 20};
        sumaElementow(tablica);
    }

    private static void sumaElementow(int[] tablica) {
        int suma = 0;
        for (int i = 0; i < tablica.length; i++) {
            suma += tablica[i];
        }
        System.out.println("suma: " + suma);
        System.out.println("srednia: " + (double) suma / tablica.length); // rzutowanie, zmiana na chwile typu z int na double
    }
}
