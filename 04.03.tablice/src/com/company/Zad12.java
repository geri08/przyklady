package com.company;

/**
 * Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę. Metoda ma usunąć z podanej
 * tablicy element o wybranym indeksie i zwrócić nową tablicę.
 * > Dla `([1, 2, 3, 4, 5],  2)`
 * > zwróci `[1, 2, 4, 5]`
 */
public class Zad12 {
    public static void main(String[] args) {
        int[] tablica = new int[]{1, 2, 3, 4, 5};
        wyswietl(tablica);
        System.out.println();
        wyswietl(usun(tablica, 2));
    }

    private static void wyswietl(int[] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + ", ");
        }
    }

    private static int[] usun(int[] tablica, int indeks) {
        int[] nowatablica = new int[tablica.length - 1];
        int licznik = 0;
        for (int i = 0; i < nowatablica.length; i++) {
            if (indeks == i) {
                licznik++;
            }
            nowatablica[i] = tablica[licznik];
            licznik++;
        }
        return nowatablica;
    }
}
