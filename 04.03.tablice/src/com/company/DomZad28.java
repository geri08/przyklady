package com.company;
/*
Utwórz metodę, która przyjmuje tablicę liczb i zwraca rozmiar tablicy po usunięciu duplikatów

Dla [-7, 8, 8, 0, 2, 7, 2] (ma 7 elementów)
zwróci 5 (bo tablica bez duplikatów to [-7, 8, 0, 2, 7])
 */

public class DomZad28 {
    public static void main(String[] args) {
        int tablica[] = new int[]{ 1,2,2,3,4,5,6} ;
        System.out.println(bezDuplikatu(tablica));
    }

    private static int bezDuplikatu(int[] tablica) {
        int nowyRozmiar = 0;

        for (int i = 0; i < tablica.length - 1; i++) {
            if (tablica[i] != tablica[i + 1]) {
                nowyRozmiar++;
            }
        }
        return nowyRozmiar;
    }
}
