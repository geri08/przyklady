package com.company;
/*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma zwrócić ile elementów (idąc po kolei od lewej) należy zsumować by przekroczyć podany (jako drugi) parametr
dla ([1,2,3,4,5,6], 9)
należy zwrócić 4
 */

public class DomZad25 {
    public static void main(String[] args) {
        int tablica[] = new int[]{1, 2, 3, 4, 5, 6};
        int koniec =14;
        System.out.println(sprawdz(tablica,koniec));
    }

    private static int sprawdz(int[] tablica, int koniec) {
        int suma=0;
        for (int i = 0; i < tablica.length; i++) {
            suma+=tablica[i];
            if (suma>koniec){
                return tablica[i];
            }
        }
        return -1;
    }
}