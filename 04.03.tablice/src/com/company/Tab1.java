package com.company;

public class Tab1 {
    public static void main(String[] args) {
//        int[] tablica = new int[5];         deklaruje dlugosc tablicy
        int [] tablica = new int[] {1,2,3,4,5,6}; // deklaruje tablice o konkretnej dlugosci z przypisnaymi elementami
        tablica[2] = 5;
        System.out.println("element 3: " +tablica[2]);
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i]+ ", ");
        }
    }

}
