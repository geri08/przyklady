package com.company;

/*
Utwórz metodę, która jako parametr przyjmuje
tablicę i zwraca liczbę unikalnych elementów
 */
public class DomZad22 {
    public static void main(String[] args) {
        int tablica[] = new int[]{7,3,2, 5, 2, 1, 2, 4, 1};
        System.out.println(ileUnikalnych(tablica));
    }

    private static int ileUnikalnych(int[] tablica) {
        int licznik = tablica.length;
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica.length; j++) {
                if (i != j) {
                    if (tablica[i] == tablica[j]) {
                        licznik--;
                        break;
                    }
                }
            }
        }
        return licznik;
    }
}
