package com.company;
/*Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca sumę elementów z pominięciem największego.

dla [1, 2, 3, 10, 4, 5, 6, 11]
zwróci 31 (pomija 11)
*/

public class DomZad23 {
    public static void main(String[] args) {
        int tablica[] = new int []{1,2,3,10,4,5,6,11};
        System.out.println(suma(tablica));
    }

    private static int suma(int[] tablica) {
        int suma=0;
        int najwieksza = tablica[0];
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i]>= najwieksza){
                najwieksza= tablica[i];
            }
            suma+=tablica[i];
        }
        return suma-(najwieksza);
    }
}
