package com.company;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca drugą największą liczbę w tablicy.

dla [1, 2, 3, 4, 5] zwróci 4
 */

public class Dom17 {
    public static void main(String[] args) {
        int[] tablica = new int[]{11, 3, 4, 5, 22, 8, 7, 9};
        sprawdz(tablica);
    }

    private static void sprawdz(int[] tablica) {
        int wieksza =0;
                int mniejsza=0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] >= wieksza) {
                mniejsza = wieksza;
                wieksza = tablica[i];}
                else if(tablica[i]>=mniejsza){
                    mniejsza=tablica[i];}
                }

        System.out.println(mniejsza);
    }

}