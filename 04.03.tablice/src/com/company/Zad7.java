package com.company;
/*
*ZADANIE #7*
Utwórz metodę, która przyjmuje dwa parametry -tablicę oraz liczbę.
Metoda ma zwrócić informację (jako wartość logiczna) czy dana liczba znajduje się w tablicy. (edited)
 */
public class Zad7 {
    public static void main(String[] args) {
        int[] tablica = new int[4];
        tablica[0] = 10;
        tablica[1] = 22;
        tablica[2] = 2;
        tablica[3] = 3;
        int szukana = 3;
        boolean wyswielt = czySieZnajduje(tablica, szukana);
        System.out.println(wyswielt);
    }

    private static boolean czySieZnajduje(int[] tablica, int szukana) {
        for (int i = 0; i < tablica.length; i++) {
            if (szukana == tablica[i]) {
                return true;
            }
        }
        return false;
    }
}
