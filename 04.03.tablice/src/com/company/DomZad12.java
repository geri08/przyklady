package com.company;
/*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz wartośc logiczną. Gdy drugi parametr ma wartość
true metoda ma zwrócić największa liczbę z tablicy. Gdy parametr będzie miał
wartość false, metoda ma zwrócić najmniejszą liczbę z tablicy.
 */

public class DomZad12 {
    public static void main(String[] args) {
        int[] tablica = new int[]{3, 6, 32, 32, 1, -1};

        sprawdz(tablica, false);
    }

    private static void sprawdz(int[] tablica, boolean warunek) {
        if (warunek == true) {
            najwieksza(tablica);
        } else {
            najmniejsza(tablica);
        }

    }

    private static void najmniejsza(int[] tablica) {
        int mniejsza = tablica[0];
        for (int i = 1; i < tablica.length; i++) {
            if (tablica[i] <= mniejsza) {
                mniejsza = tablica[i];
            }
        }
        System.out.println(mniejsza);

    }

    private static void najwieksza(int[] tablica) {
        int wieksza = tablica[0];
        for (int i = 1; i < tablica.length; i++) {
            if (tablica[i] >= wieksza) {
                wieksza = tablica[i];
            }
        }
        System.out.println(wieksza);

    }
}
