package com.company;
/*
Utwórz metodę, która przyjmuje tablicę liczb i zwraca nową tablicę w której wszystkie ujemne liczby będą w lewej części,
 a wszystkie dodanie będą w prawej części. Liczby ujemne i dodatnie mogą być w dowolnej kolejności
 (tzn. 1,2,3 czy 3,2,1 są jak najbardziej poprawne)

Dla [1, 2, -6, -9, 11, 0, -2]
zwróci [-6, -9, -2, 0, 11, 2, 1].
 */

import java.util.Arrays;

public class DomZad29 {
    public static void main(String[] args) {
        int tablica[] = new int[]{1, 2, -6, -9, 11, 0, -2,5,-4};
        System.out.println(Arrays.toString(uporzadkowania(tablica)));
    }

    private static int[] uporzadkowania(int[] tablica) {
        int licznik = getUjemne(tablica);
        int poukladania[] = new int[tablica.length];
        for (int i = 0, j = 0; i < tablica.length; i++) {
            if (tablica[i] < 0) {
                poukladania[j] = tablica[i];
                j++;
            } else {
                poukladania[licznik] = tablica[i];
                licznik++;
            }
        }
        return poukladania;
    }

    private static int getUjemne(int[] tablica) {
        int licznik = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] < 0) {
                licznik++;
            }
        }
        return licznik;
    }

}
