package com.company;

import static java.lang.System.out;

/*
Utwórz metodę, która przyjmuje dwie tablice. Metoda ma zwrócić sumę podanych tablic.
To znaczy element na pozycji 0 z pierwszej tablicy, dodajemy do elementu na pozycji 0 z drugiej tablicy

[1, 2, 3, 4]
[5, 6, 7, 8]
[6, 8, 10, 11]
 */
public class DomZad18 {
    public static void main(String[] args) {
        int[] pierwsza = new int[]{1, 2, 3, 4};
        int druga[] = new int[]{5, 6, 7, 8};
        wypisz(sumaTablic(pierwsza, druga));
    }

    private static void wypisz(int[] suma) {
        for (int i = 0; i < suma.length; i++) {
            System.out.println(suma[i] + ", ");
        }
    }

    private static int[] sumaTablic(int[] pierwsza, int[] druga) {
        int suma[] = new int[pierwsza.length];
        for (int i = 0; i < suma.length; i++) {
            suma[i] = pierwsza[i] + druga[i];
        }
        return suma;
    }
}