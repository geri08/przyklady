package com.company;

import java.util.Arrays;

/*
Utwórz metodę, która przyjmuje tablicę liczb. Metoda ma zwrócić nową tablicę wartości logicznych (boolean),
gdzie wartości dodatnie (oraz 0) zostaną zastąpione true, a ujemne false.

Dla [2, 3, -6, 0, -7, -99]
zwróci [true, true, false, true, false, false]
 */
public class DomZad27 {
    public static void main(String[] args) {
        int tablica[] = new int[]{2, 3, -6, 0, -7, -99};
        System.out.println(Arrays.toString(zamienNaLogiczne(tablica)));
    }

    private static boolean[] zamienNaLogiczne(int[] tablica) {
        boolean nowaTablica[] = new boolean[tablica.length];
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] >= 0) {
                nowaTablica[i] = true;
            }

        }
        return nowaTablica;
    }
}
