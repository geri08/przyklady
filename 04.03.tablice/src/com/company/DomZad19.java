package com.company;

/*
Utwórz metodę, która przyjmuje dwa dwie tablice.
Metoda ma zwrócić informację czy przekazane tablice są takie same.
 */
public class DomZad19 {
    public static void main(String[] args) {
        int pierwsza[] = new int[]{1, 5, 3, 4, 6,10,9};
        int druga[] = new int[]{1, 5,3, 4, 6,9};
        boolean wynik =sprawdzCzyRowne(pierwsza, druga);
        System.out.println(wynik ? "tablice sa rowne" : " tablice nie są równe");
    }

    private static boolean sprawdzCzyRowne(int[] pierwsza, int[] druga) {
        for (int i = 0; i < pierwsza.length; i++) {
            if (pierwsza[i] != druga[i] & pierwsza.length!=druga.length) {
                return false;
            }
        }
        return true;
    }
}
