package com.company;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca różnicę
pomiędzy największym a najmniejszym elementem.
 */

public class DomZad21 {
    public static void main(String[] args) {
        int tablica[] = new int[]{11, 5, 3, 3, 4, 10, 2, 1};
        System.out.println(najwiekszaLiczba(tablica));
        System.out.println(najmniejszaLiczba(tablica));
        System.out.println(roznica(najmniejszaLiczba(tablica), najwiekszaLiczba(tablica)));
    }

    private static int roznica(int najmniejsza, int najwieksza) {
        int roznica = najwieksza - najmniejsza;
        return roznica;
    }

    private static int najmniejszaLiczba(int[] tablica) {
        int najmniejsza = tablica[0];
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] <= najmniejsza) {
                najmniejsza = tablica[i];
            }
        }
        return najmniejsza;
    }

    private static int najwiekszaLiczba(int[] tablica) {
        int najwieksza = tablica[0];
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] >= najwieksza) {
                najwieksza = tablica[i];
            }
        }
        return najwieksza;
    }
}
