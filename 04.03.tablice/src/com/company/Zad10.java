package com.company;

/**
 * Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca nową tablicę z
 * liczbami w odwrotnej kolejności. *Nie wolno utworzyć nowej tablicy - należy pracować na tej wejściowej.* (edited)
 */
public class Zad10 {
    public static void main(String[] args) {
        int[] tablica = new int[]{1, 3, 5, 8, 9,22};
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i]+ ", ");
        }
        System.out.println();
        odwroc(tablica);
    }

    private static void odwroc(int[] tablica) {
        for (int i = 0; i < tablica.length/2; i++) {
            int a = tablica[i];
            tablica[i] =tablica[tablica.length-1-i];
            tablica[tablica.length - 1 - i] = a;
        }
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i]+", ");
        }
    }
}
