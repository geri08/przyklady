package com.company;
/*
Utwórz metodę, która przyjmuje dwuwymiarową tablicę i zwraca informację czy któreś z rzędów się
powtarzają (interesuje nas sam fakt powtarzania, a nie ilośc razy, czy których z nich)
 */

public class DomZad2Wielo {
    public static void main(String[] args) {
        int[][] tablica = new int[3][];
        tablica[0] = new int[]{3, 2, 4};
        tablica[1] = new int[]{3, 2, 4};
        tablica[2] = new int[]{3, 2, 4};
        System.out.println(sprawdz(tablica));
    }

    private static boolean sprawdz(int[][] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            for (int k = 0; k < tablica.length-1; k++) {
                for (int j = 0; j < tablica[i].length; j++) {
                    int licznik =0;
                    if(i==k){
                        k++;
                    }
                    if (tablica[i][j] == tablica[k][j]) {
                    licznik++;
                    }
                    if (licznik ==3){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
