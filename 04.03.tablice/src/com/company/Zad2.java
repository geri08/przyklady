package com.company;

/*
Utwórz metodę, która przyjmuje jeden parametr (który jest liczbą wierszy )
oraz wyświetla “choinkę” nie przerywając numerowania
 */
public class Zad2 {
    public static void main(String[] args) {
        choinka(4);
    }

    private static void choinka(int w) {
        int i = 1;
        int wiersz = 0;
        while (wiersz < w) {
            int kolumny = 0;
            while (kolumny <= wiersz) {
                System.out.print(i + " ");
                i++;
                kolumny++;
            }
            System.out.println();
            wiersz++;
        }

    }
}
