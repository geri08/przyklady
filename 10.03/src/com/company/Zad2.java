package com.company;

import java.util.Random;

/*Utwórz metodę, która przyjmuje dwa parametry - długość i szerokość tablicy. Utwórz drugą metodę, który wypełni
ją wartościami losowymi. Następnie trzecia metoda wyświetla ją *wraz z sumą elementów* w danym wierszu i kolumnie
> Na przykład:
>
1 2 3 | 6
1 2 3 | 6
1 2 3 | 6
- - - + -
3 6 9 | 18

 */
public class Zad2 {
    public static void main(String[] args) {
        int szerokosc = 3;
        int dlugosc = 5;
        sumujElementy( szerokosc, dlugosc);
    }

    private static void sumujElementy(int szerokosc,int dlugosc ) {
        int[][] tablica = stworzTablice(szerokosc,dlugosc,21);
        int[] sumyKolumn =new int[dlugosc];
        for (int i = 0; i < tablica.length; i++) {
            int sumaWiersz =0;
            for (int j = 0; j <tablica[i].length ; j++) {
                sumaWiersz+=tablica[i][j];
                System.out.print(tablica[i][j]+ "\t");
                sumyKolumn[j]+= tablica[i][j];
            }
            System.out.println(" | " + sumaWiersz);
        }
        System.out.println();
        for (int i = 0; i <dlugosc ; i++) {
            System.out.print("-\t");
        }
        System.out.println();
        for (int i = 0; i < dlugosc; i++) {
            System.out.print(sumyKolumn[i] +" \t");

        }
    }
private static int[][] stworzTablice(int szerokosc, int dlugosc){
        return stworzTablice(szerokosc,dlugosc, 21);
}
    private static int[][] stworzTablice(int szerokosc, int dlugosc, int zakresLiczb) {
        int[][] tablica = new int[szerokosc][dlugosc];
        Random random = new Random();
        for (int i = 0; i < szerokosc; i++) {
            for (int j = 0; j < dlugosc; j++) {
                tablica[i][j] = random.nextInt(zakresLiczb);
            }
        }
        return tablica;
    }
}
