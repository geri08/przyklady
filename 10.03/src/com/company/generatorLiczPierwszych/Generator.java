package com.company.generatorLiczPierwszych;
/*
Utwórz klasę GeneratorLiczbPierwszych oraz zaimplementuj metody:

umożliwiającą wygenerowanie wybranej ilości liczb pierwszych. Dla 4 powinno
zwrócić tablicę zawierającą [2, 3, 5, 7], a dla 10 powinno zwrócić tablicę
zawierającą [2, 3, 5, 7, 11, 13, 17, 19, 23, 29].
zwrócenie wybranej liczby pierwszej
spradzają czy liczba podana jako parametr jest pierwsza (powinna zwracać
wartość boolean)
 */

public class Generator {
    int liczba;
    int[] tablica;

    public Generator(int ileLiczbpierwszych) {
        this.liczba=ileLiczbpierwszych;
    }

    public boolean sprawdzCzyPierwsza(int liczba) {
        for (int i = 2; i < liczba - 1; i++) {
            if (liczba % i == 0) {
                return false;
            }
        }
        return true;
    }

    public int[] stworzTablicePierwszych() {
        tablica = new int[liczba];
        int indeks = 0;
        for (int i = 2; indeks < liczba; i++) {
            if (sprawdzCzyPierwsza(i)) {
                tablica[indeks] = i;
                indeks++;
            }
        }
        return tablica;
    }

}
