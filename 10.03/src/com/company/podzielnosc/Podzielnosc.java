package com.company.podzielnosc;

public class Podzielnosc {
    private int przechwyconaLiczba;
  private   int[] tabLiczb;


    Podzielnosc() {
    }

    public Podzielnosc(int przechwyconaLiczba) {
        this.tabLiczb = zamienNaTablice(przechwyconaLiczba);
    }

   private int[] zamienNaTablice(int przechwyconaLiczba) {
        int x = przechwyconaLiczba;
        String[] tablicaNapisow = Integer.toString(x).split("");
        int[] tablicaLiczb = new int[tablicaNapisow.length];

        for (int i = 0; i < tablicaLiczb.length; i++) {
            tablicaLiczb[i] = Integer.parseInt(tablicaNapisow[i]);
        }
        return this.tabLiczb = tablicaLiczb;
    }

    boolean podzielnaPrzez2(int przechwyconaLiczba) {
        zamienNaTablice(przechwyconaLiczba);
        return tabLiczb[tabLiczb.length - 1] % 2 == 0 ? true : false;
    }

    boolean podzielnaPrzez3(int przechwyconaLiczba) {
        zamienNaTablice(przechwyconaLiczba);
        int sumaCyfr = 0;
        for (int i = 0; i < tabLiczb.length; i++) {
            sumaCyfr += tabLiczb[i];
        }
        return sumaCyfr % 3 == 0 ? true : false;
    }

    boolean podzielnaPrzez4(int przechwyconaLiczba) {
        zamienNaTablice(przechwyconaLiczba);
        if (przechwyconaLiczba > 10) {
            return (10 * tabLiczb[tabLiczb.length - 2] + tabLiczb[tabLiczb.length - 1]) % 4 == 0 ? true : false;
        }
        return przechwyconaLiczba % 4 == 0 ? true : false;
    }

    boolean podzielnaPrzez5(int przechwyconaLiczba) {
        zamienNaTablice(przechwyconaLiczba);
        if (tabLiczb[tabLiczb.length - 1] == 5 || tabLiczb[tabLiczb.length - 1] == 0) {
            return true;
        }
        return false;
    }

    boolean podzielnaPrzez6(int przechwyconaLiczba) {
        zamienNaTablice(przechwyconaLiczba);
        if (podzielnaPrzez3(przechwyconaLiczba) && podzielnaPrzez2(przechwyconaLiczba)) {
            return true;
        }
        return false;
    }

    boolean podzielnaPrzez7(int przechwyconaLiczba) {
        return przechwyconaLiczba % 7 == 0 ? true : false;
    }

    boolean podzielnaPrzez8(int przechwyconaLiczba) {
        zamienNaTablice(przechwyconaLiczba);
        int liczbaZ3Ostatnich;
        if (przechwyconaLiczba > 99) {
            liczbaZ3Ostatnich = (100 * tabLiczb[tabLiczb.length - 3]) +
                    (10 * tabLiczb[tabLiczb.length - 2]) +
                    tabLiczb[tabLiczb.length - 1];
            return liczbaZ3Ostatnich % 8 == 0 ? true : false;
        } else {
            return przechwyconaLiczba % 8 == 0 ? true : false;
        }
    }

    boolean podzielnaPrzez9(int przechwyconaLiczba) {
        zamienNaTablice(przechwyconaLiczba);
        int suma = 0;
        for (int i = 0; i < tabLiczb.length; i++) {
            suma += tabLiczb[i];
        }
        return suma % 9 == 0 ? true : false;
    }

    boolean podzielnaPrzez10(int przechwyconaLiczba) {
        if (przechwyconaLiczba < 10) {
            return false;
        } else {
            zamienNaTablice(przechwyconaLiczba);
            return tabLiczb[tabLiczb.length - 1] == 0 ? true : false;
        }
    }

    void podzielPrzezWszystkie(int przechwyconaLiczba) {

            System.out.print(przechwyconaLiczba+ "  -->  1");
            System.out.print(podzielnaPrzez2(przechwyconaLiczba)? ", 2":"");
            System.out.print(podzielnaPrzez3(przechwyconaLiczba)? ", 3":"");
            System.out.print(podzielnaPrzez4(przechwyconaLiczba)? ", 4":"");
            System.out.print(podzielnaPrzez5(przechwyconaLiczba)? ", 5":"");
            System.out.print(podzielnaPrzez6(przechwyconaLiczba)? ", 6":"");
            System.out.print(podzielnaPrzez7(przechwyconaLiczba)? ", 7":"");
            System.out.print(podzielnaPrzez8(przechwyconaLiczba)? ", 8":"");
            System.out.print(podzielnaPrzez9(przechwyconaLiczba)? ", 9":"");
            System.out.print(podzielnaPrzez10(przechwyconaLiczba)? ", 10":"");
            System.out.println();

    }
void podajZbiorOdDo(int pierwsza,int druga){
    for (int przechwyconaLiczba = pierwsza; przechwyconaLiczba <= druga; przechwyconaLiczba++) {
        podzielPrzezWszystkie(przechwyconaLiczba);
    }
}

    void wyswietlTablice() {
        System.out.print("[ ");
        for (int i : tabLiczb) {
            System.out.print(i + ", ");
        }
        System.out.println("\b\b ]");
    }
}
