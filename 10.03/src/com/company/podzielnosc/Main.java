package com.company.podzielnosc;
/*
Utwórz klasę `Podzielnosc` przyjmujacą w konstruktorze liczbę do sprawdzenia. Liczba powinna być
przechowywana w formie tablicy wartości typu `int` (czyli liczba `1234` ma zostać zamieniona na
tablicę `[1, 2, 3, 4]`).<br>
By konwertować liczbę na wartości, możesz najpier zamieniać liczbę na tablicę napisów (drugia linia),
a następnię pętlą rzutować każdy ze znaków na liczbę (czyli napis `"5"` na liczbę `5`):
 */

public class Main {
    public static void main(String[] args) {
        Podzielnosc podzielnosc = new Podzielnosc();
//        podzielnosc.podzielPrzezWszystkie(1536);
        podzielnosc.podajZbiorOdDo(10,40);
    }
}
