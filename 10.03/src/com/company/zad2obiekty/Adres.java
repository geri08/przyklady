package com.company.zad2obiekty;
/*
Utwórz klasę `Address` (`Adres`) posiadającą
*pola reprezentujące:*
* ulicę
* numer domu
* numer mieszkania
* kod pocztowy
* miasto
*
*następujące kostruktory:*
* bezparametrowy
* 3-parametrowy (przyjmujący pierwsze trzy pola)
* 5-parametrowy (przyjmujący wszystkie pola)
*
*metody zwracające:*
* pełen adres
* ulicę (wraz z numerami) (edited)
 */

public class Adres {
    String ulica, miasto;
    int numerDomu, mieszkanie;
    String kodPocztowy;

    Adres() {
    }

    public Adres(String ulica, int numerDomu, int mieszkanie) {
        this.ulica = ulica;
        this.numerDomu = numerDomu;
        this.mieszkanie = mieszkanie;
    }

    public Adres(String ulica, String miasto, int numerDomu, int mieszkanie, String kodPocztowy) {
        this.ulica = ulica;
        this.miasto = miasto;
        this.numerDomu = numerDomu;
        this.mieszkanie = mieszkanie;
        this.kodPocztowy = kodPocztowy;

    }
    String pelenAdres(){
        return  String.format("%s %s %s %s %s", kodPocztowy,miasto,ulica,numerDomu,mieszkanie);
    }

}
