package com.company.zadanie11d;
/*
Utwórz klasę `Konto` służącą do odbsługi rachunku bankowego
*pola:*
* stan konta
* właściciel konta

*metody:*
* umożliwiające wpłacanie na rachunek
* umożliwiające wypłacanie z rachunku
* pobranie numeru konta (edited)
 */

public class Konto {
    private int stanKonta;
    private String wlasciciel;
    private static int liczbarachunkowWBanku = 0;
    private int nrRachunku;

    public Konto(int stanKonta, String wlasciciel) {
        this.stanKonta = stanKonta;
        this.wlasciciel = wlasciciel;
        liczbarachunkowWBanku++;
        nrRachunku=liczbarachunkowWBanku;
    }

    public void wplataNaRachunek(int wplata) {
        stanKonta += wplata;
    }

    public void wyplataZRachunku(int wyplata) {
        if (stanKonta >= wyplata) {
            stanKonta -= wyplata;
        } else {
            System.out.println("kwota przewyższa saldo na rachunku");
        }

    }

    void wyswietlStanKonta() {
        System.out.println("twój stan konta: "+stanKonta+ " ,a jego numer to: "+ nrRachunku);
    }
}
