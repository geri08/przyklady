package com.company.analizator;
/*
Utwórz klasę Analizator przyjmująca w konstruktorz napis (typ String)
oraz zaimplementuj metody umożliwiające:

policzenie liczy samogłosek
policzenie liczy spółgłosek
zwracająca długość napisu
zwrócenie napisu w odwrotnej kolejności (Dla pies zwróci seip)
zwrócenie litery pod wybranym indeksem
zwrócenie informacji ile razy występuje (podana jako parametr) litera
sprawdzenie czy wybrany wyraz jest palindromem
przesunięcie liter o wybraną długość (dla telewizor i długości 3 powinno zwrócić zortelewi)
 */

public class Analizator {
    private char[] tablica;

    public Analizator(String napis) {
        tablica = new char[napis.length()];
        zamienNaTablice(napis);
    }

    private char[] zamienNaTablice(String napis) {
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = napis.charAt(i);
        }
        return this.tablica = tablica;
    }

    void wyswietltablice() {
        for (int i = 0; i < tablica.length; i++) {
            System.out.println(tablica[i]);
        }
    }

    public int policzSamogloski() {
        int licznik = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] == 'a' ||
                    tablica[i] == 'e' ||
                    tablica[i] == 'y' ||
                    tablica[i] == 'u' ||
                    tablica[i] == 'i' ||
                    tablica[i] == 'o') {
                licznik++;
            } else {
            }
        }
        return licznik;
    }

    public int policzSpolgloski() {
        return tablica.length - policzSamogloski() - policzSpacje();
    }

    public int zwrocDlugoscNapisu() {
        return tablica.length;
    }

    public char[] napisOdTylu() {
        char nowaTablica[] = new char[tablica.length];
        int j = tablica.length - 1;
        for (int i = 0; i < nowaTablica.length; i++, j--) {
            nowaTablica[i] = tablica[j];
        }
        return nowaTablica;
    }

    public char zwrocZIndexu(int indeks) {
        return tablica[indeks];
    }

    public int ileRazyWystepujeLitera(char litera) {
        int licznik = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (litera == tablica[i]) {
                licznik++;
            } else {
            }
        }
        return licznik;
    }

    public boolean sprawdzCzyPalindrom() {
        char[] nowaTablica = usunSpacjeZTablicy();

        int j = nowaTablica.length - 1;
        for (int i = 0; i < nowaTablica.length / 2; i++, j--) {
            if (nowaTablica[i] != nowaTablica[j]) {
                return false;
            }
        }
        return true;
    }

    public char[] przesunElementy(int przesuniecie) {
        char[] nowaTablic = new char[tablica.length];
        int j = przesuniecie;
        for (int i = 0; i < nowaTablic.length - przesuniecie; i++) {
            nowaTablic[j] = tablica[i];
            j++;
        }
        int g = przesuniecie;
        for (int i = 0; i < przesuniecie; i++) {
            nowaTablic[i] = tablica[tablica.length - g];
            g--;
        }
        return nowaTablic;
    }

    private int policzSpacje() {
        int iloscSpacji = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] == ' ') {
                iloscSpacji++;
            }
        }
        return iloscSpacji;
    }

    private char[] usunSpacjeZTablicy() {
        char[] nowaTablica = new char[tablica.length - policzSpacje()];
        for (int i = 0, j = 0; i < nowaTablica.length; i++, j++) {
            if (tablica[j] == ' ') {
                j++;
            }
            nowaTablica[i] = tablica[j];
        }
        return nowaTablica;
    }
}
