package com.company.zadanie11e;
/*
*ZADANIE #5 - część DRUGA*
Utwórz klasę `Firma` posiadającą listę (`ArrayList()`) osób
(*z powyższego zadania*) oraz metody umożliwiające:
* wyświetlenie listy pracowników
* zwrócenie pracownika który zarabia najwięcej
* zwrócenie najstarszego pracownika
* zwrócenie liczby pracowników starszych niż podana wartość
* przyznanie podwyżki procentowej (np. podnieść wszystkim o 10%)
* przyznanie podwyżki kwotowej (np. wszystkim o 500 zł) */

import java.util.ArrayList;
import java.util.List;

public class Firma {
    private List<Pracownik> listaPracownikow = new ArrayList<>();

//    public Firma(List<Pracownik> listaPracownikow) {
//        this.listaPracownikow = listaPracownikow;
//    }

    void dodajPracownika(Pracownik pracownik1) {
        listaPracownikow.add(pracownik1);
    }

    void wyswietlListePracownikow() {
        for (int i = 0; i < listaPracownikow.size(); i++) {
            listaPracownikow.get(i).imieNazwisko();
            System.out.println();
        }
    }
    Pracownik najlepiejOpłacany(){
        int najwiekszaPensja =listaPracownikow.get(0).getPensja();
        int index =0;
        for (int i = 1; i < listaPracownikow.size(); i++) {
            Pracownik p = listaPracownikow.get(i);
            if(najwiekszaPensja<p.getPensja())
            najwiekszaPensja =p.getPensja();
            index = 1;
        }
        return listaPracownikow.get(index);
    }
    void domuDacPrezent(int procentPodwyzki){
        for (int i = 0; i < listaPracownikow.size(); i++) {
            Pracownik p =listaPracownikow.get(i);
           p.setPensja(p.getPensja()*procentPodwyzki/100 + p.getPensja());
        }
    }
    void podniesWynagrodzenieDo(int kwotaMin){
        for (int i = 0; i < listaPracownikow.size(); i++) {
            Pracownik p =listaPracownikow.get(i);
            if(p.getPensja()<kwotaMin){
                p.setPensja(kwotaMin);
        }

        }
    }
}
