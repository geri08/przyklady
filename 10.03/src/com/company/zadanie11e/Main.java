package com.company.zadanie11e;

/*ZADANIE #5*
Utwórz klasę `Pracownik` posiadającą
*pola reprezentujące:*
* imie
* drugie imie
* nazwisko
* wiek
* płeć (`true` = kobieta, `false` = mężczyzna)
* pensję
* adres (z poprzedniego zadania)
*gettery & settery*
*następujące kostruktory:*
* bezparametrowy
* 2-parametrowy (przyjmujący imię, nazwisko i płeć)
*metody zwracające:*
* imie, drugie imie (jeśli istnieje!) i nazwisko (jako jeden string)
* płec w formie tekstowej (tzn. `Kobieta` lub `Mężczyzna`)
* liczbę lat pozostałych do emerytury (dla `Kobiet` 60, dla `Mężczyzn` 65) */
public class Main {
    public static void main(String[] args) {
        Pracownik pracownik1 = new Pracownik("pawel", "wasiak", 700, 30, false);
        Pracownik pracownik2 = new Pracownik("monika", "zielonka", 900, 32, true);
        Pracownik pracownik3 = new Pracownik("artu", "Walaszek", 1800, 33, false);
        Pracownik pracownik4 = new Pracownik ();
//        pracownik4.wczytajPracownika();

        //        System.out.println(pracownik1.ileDoEmerytury());
//        pracownik1.imieNazwisko();
//        System.out.println();
//        System.out.println(pracownik2.ileDoEmerytury());
//        pracownik2.imieNazwisko();
//Pracownik pracownik3 =new Pracownik();
//pracownik3.wczytajPracownika();
//pracownik3.imieNazwisko();

//        Firma firma = new Firma();
//        firma.dodajPracownika(pracownik1);
//        firma.dodajPracownika(pracownik2);
//        firma.dodajPracownika(pracownik3);
//        firma.wyswietlListePracownikow();
//        System.out.println("najlepiej opłacany jest: ");
//        firma.najlepiejOpłacany().imieNazwisko();
//        System.out.println();
//        System.out.println();
//        firma.domuDacPrezent(20);
//        firma.wyswietlListePracownikow();
//        System.out.println();
//        firma.podniesWynagrodzenieDo(1350);
//        firma.wyswietlListePracownikow();
    }
}
