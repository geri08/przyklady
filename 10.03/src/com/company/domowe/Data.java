package com.company.domowe;

public class Data {
    int dzien, miesiac, rok;
    String separator;


    Data() {
    }

    Data(int dzien, int miesiac, int rok) {
        this.dzien = dzien;
        this.miesiac = miesiac;
        this.rok = rok;
    }

    boolean ustawDzien(int dzien) {
        if (dzien > 0 && dzien < 32) {
            this.dzien = dzien;
            return true;
        }
        return false;
    }

    boolean ustawMiesiac(int miesiac) {
        if (miesiac > 0 && miesiac < 13) {
            this.miesiac = miesiac;
            return true;
        }
        return false;
    }

    boolean ustawRok(int rok) {
        if (rok > 1) {
            this.rok = rok;
            return true;
        }
        return false;
    }

    String wyswietlDate() {
        separator = "-";
        return wyswietlDate(dzien, miesiac, rok, separator);
    }

    String wyswietlDateOdRoku(int rok, int miesiac, int dzien) {
        separator = "-";
        return wyswietlDate(rok, miesiac, dzien, separator);
    }

    public String wyswietlDate(int dzien, int miesiac, int rok, String sep) {
        return String.format("%s%s%s%s%s", dzien, sep, miesiac, sep, rok);
    }
}
