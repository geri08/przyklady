package com.company.domowe;
/*
Utwórz klasę `Data` posiadającą
*pola reprezentujące:*
- dzień
- miesięc
- rok

*następujące kostruktory:*
- bezparametrowy
- 3-parametrowy (przyjmujący wszystkie parametry)

*metody umożliwiające:*
- ustawienie dnia (wraz ze sprawdzaniem poprawności) - zwracająca typ `boolean`
- ustawienie miesiąca (wraz ze sprawdzaniem poprawności) - zwracająca typ `boolean`
- ustawienie roku (wraz ze sprawdzaniem poprawności) - zwracająca typ `boolean`
- wyświetlenie daty w formacie `dzien-miesiac-rok` (opcjonalnie z możliwością
ustawienia separatora)
- wyświetlenie daty w formacie `rok-miesiac-dzien` (opcjonalnie z możliwością
ustawienia separatora)
- przesunięcie daty o wybraną liczbę dni (edited)
 */

public class Glowna {
    public static void main(String[] args) {
        Data data = new Data();
        int dzien=40;
        System.out.println(data.ustawDzien(dzien));
        data.miesiac=10;
        data.rok=2000;
//        System.out.println(data.wyswietlDate(dzien,11,2005));
//        System.out.println(data.wyswietlDate(data.dzien,data.miesiac,data.rok));
        System.out.println(data.wyswietlDateOdRoku(2000, 2, 1));
        System.out.println(data.wyswietlDateOdRoku(data.rok,data.miesiac,data.dzien));
        Data data2 = new Data(13,13,2000);
        System.out.println(data2.wyswietlDate());
    }

}
