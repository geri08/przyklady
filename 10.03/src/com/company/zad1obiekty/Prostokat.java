package com.company.zad1obiekty;
/*
Utwórz klasę `Rectangle` (`Prostokąt`) posiadającą
*pola reprezentujące:*
- dłuższy bok
- krótszy bok

*metody służące do:*
- policzenia obwodu
- policzenia pola powierzchni
- porównania czy pola powierzchni dwóch przekazanych prostokątów są takie same (zwracająca wartość `boolean`) (edited)
 */

public class Prostokat {
    int dluzszyBok;
    int krotszyBok;

    Prostokat(int dluzszyBok, int krotszyBok){
        this.dluzszyBok = dluzszyBok;
        this.krotszyBok = krotszyBok;
    }
    int obwod(){
        return  2*dluzszyBok + 2*krotszyBok;
    }
    int policzPole(){
        return dluzszyBok*krotszyBok;
    }
    static boolean ktoryWiekszy(Prostokat p1, Prostokat p2){
        if (p1.policzPole()== p2.policzPole()) {
            return true;
        }else{
            return false;
        }
    }

}
