package com.company.zad1obiekty;
/*
Utwórz klasę `Rectangle` (`Prostokąt`) posiadającą
*pola reprezentujące:*
- dłuższy bok
- krótszy bok

*metody służące do:*
- policzenia obwodu
- policzenia pola powierzchni
- porównania czy pola powierzchni dwóch przekazanych prostokątów są takie same (zwracająca wartość `boolean`) (edited)
 */

public class Zadanie {
    public static void main(String[] args) {
        Prostokat pl = new Prostokat(5,3);
        System.out.println("dluzszy bok to: " + pl.dluzszyBok);
        System.out.println("obwod to:" + pl.obwod());
        System.out.println("pole pierwszego to: " + pl.policzPole());

        Prostokat drugiProstokat = new Prostokat(3,5);
        System.out.println("obwód drugiego to: "+ drugiProstokat.obwod());
        System.out.println("pole drugiego to: "+ drugiProstokat.policzPole());
        boolean wynik =Prostokat.ktoryWiekszy(pl,drugiProstokat);
        System.out.println(wynik);
    }
}


