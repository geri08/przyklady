package com.company.zadanie11b;
/*
*ZADANIE #2*
Utwórz klasę `Tablica` służącą do obsługi tablicy jednowymiarowej z możliwością
dynamicznej zmiany rozmiaru.
*konstruktor:
* jednoparametrowy (przyjmujący początkowy rozmiar tablicy)

*pole:
* będące tablicą, ale z modyfikatorem `private`

*metody umożliwiające:
+++++ Wstawienie elementu na koniec
+++++Zwrócenie rozmiar tablicy
+++++ Wyświetlenie tablicy
+++++ Rozszerzenie tablicy o wybraną liczbę pozycji na końcu (domyślnie o `1`)
++++++ Wstawienie elementu na początek
+++++ Wstawienie elementu pod wybranym indeksem
++++++ Usunięcie elementu pod wybranym indeksem
++++++ Zwrócenie największego elementu w tablicy (edited)
 */
import java.util.Random;
public class Tablica {
    private int[] tablica;

    Tablica(int rozmiar) {
        tablica = new int[rozmiar];
    }

    int najwiekszaLiczba() {
        return tablica[indeksNajwiekszejliczby()];
    }

    int indeksNajwiekszejliczby() {
        int indeks = 0;
        int najwiekszaLiczba = tablica[0];
        for (int i = 0; i < tablica.length; i++) {
            if (najwiekszaLiczba < tablica[i]) {
                najwiekszaLiczba = tablica[i];
                indeks = i;
            }
        }
        return indeks;
    }

    void usunPodIndeksem(int indeks) {
        int[] nowaTablica = new int[tablica.length - 1];
        int przesuniecie = 0;
        for (int i = 0; i < nowaTablica.length; i++) {

            if (i == indeks) {
                przesuniecie++;
            }
            nowaTablica[i] = tablica[i + przesuniecie];
        }
        this.tablica = nowaTablica;
    }

    private void rozszerzTablice() {
        rozszerzTablice(1, true);
    }

    private void rozszerzTablice(int wartosc, boolean czyNakoncu) {
        int[] nowaTablica = new int[tablica.length + wartosc];
        int przesuniecie;
        if (czyNakoncu) {
            przesuniecie = 0;
        } else {
            przesuniecie = wartosc;
        }

        for (int i = 0; i < tablica.length; i++) {
            nowaTablica[i + przesuniecie] = tablica[i];
        }
        tablica = nowaTablica;
    }

    void wstawNaKoniec(int wartosc) {
        rozszerzTablice();
        wstawElement(tablica.length - 1, wartosc);
    }

    void wstawNaPoczatek(int wartosc) {
        rozszerzTablice(1, false);
        wstawElement(0, wartosc);

    }

    void wypelnijLosowo() {
        Random random = new Random();
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(11);
        }
    }

    void wstawElement(int index, int liczba) {
        tablica[index] = liczba;
    }

    void wyswietlTablice() {
        System.out.print("[ ");
        for (int i : tablica) {
            System.out.print(i + ", ");
        }
        System.out.println("\b\b ]");
    }

    int zwrocRozmiar() {
        return tablica.length;
    }
}
