package com.company.zadanie11b;

public class Glowna {
    public static void main(String[] args) {
        Tablica tablica = new Tablica(5);
        tablica.wyswietlTablice();

        tablica.wypelnijLosowo();
        tablica.wyswietlTablice();

        tablica.wstawElement(2, 9);
        tablica.wyswietlTablice();


        tablica.wstawNaKoniec(40);
        tablica.wyswietlTablice();
//
////        tablica.rozszerzTablice(2,true);     poniewaz ustawilismy private
////        tablica.wyswietlTablice();           brak mozliwosci recznej zmiany
//
        tablica.wstawNaPoczatek(30);
        tablica.wyswietlTablice();
        tablica.usunPodIndeksem(2);
        tablica.wyswietlTablice();
        System.out.println(tablica.najwiekszaLiczba());
        System.out.println(tablica.indeksNajwiekszejliczby());
    }
}
