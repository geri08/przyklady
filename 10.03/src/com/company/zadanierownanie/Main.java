package com.company.zadanierownanie;

public class Main {
    public static void main(String[] args) {
        Rownanie rownanie = new Rownanie(3,4,5);

        System.out.println("wynik rownania a^2 + b^3 + c^4 = " + rownanie.wykonajRownanie());
//        rownanie.a=5;                     jak ustawie private w drugiej klasie to brak możliwości jej modyfikacji
        System.out.println("wynik rownania a^2 + b^3 + c^4 = " + rownanie.wykonajRownanie());
        System.out.println("a wynosi : " +rownanie.getA());
        System.out.println(rownanie.getB());
        rownanie.setB(9);
        System.out.println(rownanie.getB());
        System.out.println(rownanie.porownajWartosci(699));
    }
}
