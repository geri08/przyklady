package com.company.zadanierownanie;
/*
*ZADANIE #1*
Utwórz klasę `Rownanie` służącą do policzenia równania `a^2 + b^3 + c^4`. Klasa
powinna zawierać:
>*pola:*
>* `a`, `b`, `c`
>
>*kostruktory:*
>* bezparametrowy
>* 3-parametrowy
>
>*metody:*
>* liczaca wartość równania
>* przyjmującą liczbę a następnie zwracająca informację (`boolean`)
czy wartość równania przekroczyła podaną liczbą (jako parametr)
 */

public class Rownanie {
    private int a, b, c;

    Rownanie() {
    }

    public boolean porownajWartosci(int wartosc) {
        return wykonajRownanie() < wartosc; // skrocony if, poda wartosc rownania true lub false

    }

    public Rownanie(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double wykonajRownanie() {
        return Math.pow(a, 2) + Math.pow(b, 3) + Math.pow(c, 4);
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public void setB(int b) {
        this.b = b;
    }

}
