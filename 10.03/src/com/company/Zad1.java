package com.company;
/*
Utwórz metodę, która przyjmuje dwa parametry - długość i szerokość tablicy, a następnie zwraca nowo utworzoną,
dwuwymiarową tablicę wypełnioną losowymi wartościami. Utwórz drugą metodę do wyświetlania zwróconej tablicy.
 */

import java.util.Random;

public class Zad1 {
    public static void main(String[] args) {
        int[][] wyswietl = wylosuj(3, 5);
        wyswietlTablice(wyswietl);
    }

    private static void wyswietlTablice(int[][] wyswietl) {
        for (int i = 0; i < wyswietl.length; i++) {
            for (int j = 0; j < wyswietl[i].length; j++) {
                System.out.print(wyswietl[i][j] + "\t");
            }
            System.out.println();
        }
    }

    private static int[][] wylosuj(int piewrsza, int druga) {
        int[][] tablica = new int[piewrsza][druga];
        for (int i = 0; i < piewrsza; i++) {
            for (int j = 0; j < druga; j++) {
                Random random = new Random();
                tablica[i][j] = random.nextInt(10);
            }
        }
        return tablica;
    }
}
