package com.company.zadanie11c;

import java.util.Scanner;

/*
*ZADANIE #3*
Utwórz klasę `Pobieranie` służącą do pobierania danych z konsoli.
*metody:*
* `pobierzInt()`
* `pobierzString()`

Dodaj możliwość przekazania wartości logicznej, od której zależy wyświetlenie komunikatu
na konsoli (np. `Podaj wartość typu String: `). Użytkownik może zadeklarować obiekt nowo
utworzonej klasy u siebie w projekcie:

Pobieranie p = new Pobieranie();
p.pobierzInt(true)
uzytkownik podaje ile licz chce wczytac
 */
public class Pobieranie {
    private Scanner myScanner;

    Pobieranie() {
        myScanner = new Scanner(System.in);
    }

    public int pobierzInt(boolean info) {
        if (info) {
            System.out.print("podaj wartosc typu int: ");
        }
        int value = myScanner.nextInt();
        myScanner.nextLine();
        return value;
    }

    public String pobierzString(boolean info) {
        if (info) {
            System.out.print("\npodaj wartość typu String: ");
        }
        String value = myScanner.nextLine();
        myScanner.nextLine();
        return value;
    }

    public int[] pobierzTabliceInt(int size) {
        int[] myInts = new int[size];
        for (int i = 0; i < myInts.length; i++) {
            myInts[i] = pobierzInt(true);
        }
        return myInts;

    }

    public String[] pobierzTabliceString(int size) {
        String[] myString = new String[size];
        for (int i = 0; i < size; i++) {
            myString[i] = pobierzString(true);
        }
        return myString;
    }
}
