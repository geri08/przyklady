package com.company;

import java.util.Scanner;

public class Zad6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        System.out.println("podaj liczbe co którą chcesz wyświetlać :");
//        int skok = scanner.nextInt();


        System.out.println("podaj liczbę krańcową przedziału :");
        int koniec = scanner.nextInt();
        int skok = 2;
        petla(skok, koniec);
    }

    private static void petla(int skok, int koniec) {
        for (int i = 0; i <= koniec; i += skok) {
            System.out.print(i + " ");
        }
    }
}
