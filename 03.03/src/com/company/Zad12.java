package com.company;

import java.util.Scanner;

public class Zad12 {
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println(" podaj znak którym chcesz uzupełnić: ");
//        String znak = scanner.nextLine();

        prostokat(3, 3);
    }

    private static void prostokat(int wys, int szer) {
        prostokat(wys, szer, "x");
    }

    private static void prostokat(int wys, int szer, String znak) {
        for (int i = 0; i < wys; i++) {
            for (int j = 0; j < szer; j++) {
                if (i == j) {
                    System.out.print("o");
                } else {
                    System.out.print(znak);
                }
            }
            System.out.println();
        }
    }
}
