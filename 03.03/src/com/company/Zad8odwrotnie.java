package com.company;
import java.util.Scanner;
public class Zad8odwrotnie {
/*
utwórz metode do której przekazujesz dwa parametry(np min max) a
nastepnie wysietlasz wszystkie liczby z podanego zakresu
 */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj początek zakresu: ");
        int pocz = scanner.nextInt();
        System.out.print("podaj koniec zakresu: ");
        int kon = scanner.nextInt();
        if (kon < pocz) {
            petla(kon, pocz);
        } else {
            petla(pocz, kon);
        }
    }

    private static void petla(int pocz, int kon) {
        for (int i = kon; i >= pocz; i--) {
            System.out.print(i + " ");
        }
    }
}

