package com.company;

/*Utwórz metodę, do której przekazujesz dwa parametry. Metoda ma wyświetlić wszystkie potęgi pierwszej liczby do momentu przekroczenia drugiej liczby.
> dla `3, 100` wyświetli:
>
>0 -> 1
>1 -> 3
>2 -> 9
>3 -> 27
>4 -> 81 (kolejny krok dałby 243, więc przekroczyłby 100)

 */
public class Zad10 {
    public static void main(String[] args) {
        petla(3, 100);
        for (int i = 0; i <10 ; i++, System.out.print(i)) ;

        }


    private static void petla(int a, int b) {
        int suma = 1;
        for (int i = 0; suma < b; i++) {

            System.out.printf(" %s do %s --> %s \n", a, i, suma);
            suma = suma * a;
        }

    }
}
