package com.company;

/*
utwórz metodę, która przyjmuje  jeden parametr i wyświetla wszystkie potęgi 2 aż do tej liczby
 */
public class Zad1a {
    public static void main(String[] args) {
        int liczba = 20;
        potega(liczba);
    }

    private static void potega(int liczba) {

        int i = 1;
        do {
            System.out.println(i);
            i *= 2;
        }
        while (i < liczba)
                ;
    }

//        int i=1;
//        while (i<=liczba){
//            System.out.println(i);
//            i*=2;
//        }

//        for (int i = 1; i < liczba; i *= 2) {
//            System.out.println(i + " ");
//
//        }
}

