package com.company;
/*
utwórz metode do której przekazujesz dwa parametry(np min max) a
nastepnie wysietlasz wszystkie liczby z podanego zakresu
 */

import java.util.Scanner;

public class Zad8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj początek zakresu: ");
        int pocz = scanner.nextInt();
        System.out.print("podaj koniec zakresu: ");
        int kon = scanner.nextInt();
        if (kon < pocz) {
            petla(kon, pocz);
        } else {
            petla(pocz, kon);
        }
    }

    private static void petla(int pocz, int kon) {
        int j=0;
        for (int i = pocz; i <= kon & j<=2; i++ , j++) {
            System.out.print(i + " ");
        }
    }
}
