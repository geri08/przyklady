package com.company;

import java.util.Scanner;

public class Zad11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj liczbe: ");
        int liczba = scanner.nextInt();

        tabliczka(liczba);
    }

    private static void tabliczka(int liczba) {
        for (int i = 1; i <=10 ; i++) {
            int wynik = liczba *i;
            System.out.printf(" %s * %s = %s \n" , liczba,i,wynik);

        }
    }
}
