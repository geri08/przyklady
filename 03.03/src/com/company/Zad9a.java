package com.company;

import java.util.Scanner;

public class Zad9a {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj 1 zakres: ");
        int pocz = scanner.nextInt();
        System.out.print("podaj 2 zakres: ");
        int kon= scanner.nextInt();

        System.out.println(suma(pocz, kon));
    }

    private static int suma(int pocz, int kon) {
        int suma = 0;
        for (int i=pocz; i <= kon; i++) {
            suma = suma +i;

        }
        return suma;
    }
}
