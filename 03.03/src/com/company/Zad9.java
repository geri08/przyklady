package com.company;

public class Zad9 {
    public static void main(String[] args) {
        int par = 5;
        int wynik = petla(par);
        System.out.println();
        System.out.println(wynik);
    }

    private static int petla(int par) {
        int j = 0;
        for (int i = 0; i <= par; i++) {
            System.out.print(i +"+");
            j = i + j;
        }
        System.out.println("\b");
        return j;
    }
}
