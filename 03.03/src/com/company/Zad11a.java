package com.company;

import java.util.Scanner;

public class Zad11a {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj który element wzoru F chcesz wyświetlić: ");
        int poszukiwanyElement = scanner.nextInt();
//        ciag();
//        System.out.println(ciag(poszukiwanyElement, true));
        System.out.println(ciag(poszukiwanyElement));
    }

    private static int ciag(int poszukiwanyElement) {
        return ciag(poszukiwanyElement, false);
    }

    private static int ciag(int poszukiwanyElement, boolean print) {
        int a = 0;
        int b = 1;
        for (int i = 0; i < poszukiwanyElement; i++) {
            b = b + a;
            a = b - a;
            if (print) {
                System.out.println("a = " + a + " " + "b = " + b);
            }
        }
        return a;
    }
}
