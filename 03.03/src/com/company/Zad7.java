package com.company;

import java.util.Scanner;

public class Zad7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("podaj zakres:");
        int i = scanner.nextInt();
        petla(i);
    }

    private static void petla(int i) {
        for (; i >= 0; i -= 2) {
            System.out.print(i + " ");
        }
    }
}
