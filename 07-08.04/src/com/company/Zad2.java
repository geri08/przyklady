package com.company;
/*
*ZADANIE #2*
Utwórz metodę która przyjmuje tablicę, a następnię przy wykorzystaniu `Scannera` użytkownik może podać indeks
parametru. W przypadku indeksu poza zakresem (tzn. gdy wystąpi wyjątek) zapytaj użytkownika o indeks ponownie.
 */

import java.util.InputMismatchException;
import java.util.Scanner;

public class Zad2 {
    public static void main(String[] args) {
        int[] tablica = new int[]{21, 32, 23, 34, 433, 53, 111};
        System.out.println(podajIndeks(tablica));
    }

    private static int podajIndeks(int[] tablica) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                System.out.print("podaj numer indeksu ktory chcesz uzyskać: ");
                int indeks = scanner.nextInt();
                return tablica[indeks];
            } catch (InputMismatchException wyjatek) {
                System.out.println("podałes błedny typ danych");
            } catch (IndexOutOfBoundsException wyjatek) {
                System.out.println("wyszedłeś poza zakres tablicy, podaj jeszcze raz");
            } finally {
                scanner.nextLine();
            }
        }
    }
}
