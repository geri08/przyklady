package com.company.zadanie11;
/*
Utwórz metodę która odczytuje zawartości ze Scannera do momentu (podania pustej linii) i zapisuje wartości do pliku
 */

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            wczytajWartosc();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void wczytajWartosc() throws IOException {
        Scanner scanner = new Scanner(System.in);
        FileWriter fileWriter = new FileWriter("zapis.txt");
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        while (true) {
            System.out.println("podaj wartosc: ");
            String wartosc = scanner.nextLine();
            if (wartosc.equals("")) {
                break;
            }
            bufferedWriter.write(wartosc);
            bufferedWriter.newLine();

        }
        bufferedWriter.close();
    }
}
