package com.company.zadanie7;

public final class Kwadrat extends Ksztalt {
    int bokKwadratu;

    public Kwadrat(int bokKwadratu) {
        this.bokKwadratu = bokKwadratu;
    }

    @Override
    double policzpole() {
        return bokKwadratu*bokKwadratu;
    }
}
