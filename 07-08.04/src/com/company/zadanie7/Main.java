package com.company.zadanie7;
/*
Utwórz abstrakcyjną klasę `Ksztalt` (z polem `nazwa` oraz abstrakcyjną metodą `policzPole()`) a następnie dziedziczące klasy `Kwadrat` i `Kolo`.
 */

public class Main {
    public static void main(String[] args) {
//        Ksztalt ksztalt = new Ksztalt();
        Kolo kolo = new Kolo(5);
        Kwadrat kwadrat = new Kwadrat(10);
        System.out.println(kwadrat.policzpole());
        System.out.println(kolo.policzpole());
    }

}
