package com.company.zadanie7;

public class Kolo extends Ksztalt {
    int promien;

    public Kolo(int promien) {
        this.promien = promien;
    }

    @Override
    double policzpole() {
        nazwa = " ";
        return Math.PI * Math.pow(promien, 2);
    }
}
