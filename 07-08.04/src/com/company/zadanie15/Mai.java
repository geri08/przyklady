package com.company.zadanie15;
/*
*ZADANIE #8*
Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku, a następnie generuje plik `.html` w którym każda
linijka jest elementem listy (tzn otoczona `<li> </li>`)
 */

import java.io.*;

public class Mai {
    public static void main(String[] args) {
        String wejsciowy ="file3.txt";
        String wyjsciowy = "file4.html";
        try {
            pobierzListe(wejsciowy,wyjsciowy);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void pobierzListe(String wejscie, String wyjscie) throws IOException {
        FileReader fileReader = new FileReader(wejscie);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        FileWriter fileWriter = new FileWriter(wyjscie);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        String linia = bufferedReader.readLine();
        bufferedWriter.write("<meta charset=\"UTF-8\">");
        bufferedWriter.write("<h1>Lista zakupow:<h1/>\n");
        while (linia!=null){
            bufferedWriter.write(String.format("<li>%s</li>\n",linia));
            linia=bufferedReader.readLine();

        }
        bufferedWriter.close();

    }
}
