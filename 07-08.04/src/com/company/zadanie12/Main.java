package com.company.zadanie12;
/*
Utwórz metodę która przyjmuje dwa parametry, które są ścieżkami do plików. Metoda ma zwrócić informację czy
podane pliki są takie same. (tzn. porównać linia po linii) (edited)
 */

import java.io.*;

public class Main {
    public static void main(String[] args) {
        String sciezka1 = "plik1.txt";
        String sciezka2 = "plik2.txt";
        try {
            System.out.println(porownajPliki(sciezka1, sciezka2));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static boolean porownajPliki(String sciezka1, String sciezka2) throws IOException {
        FileReader fileReader1 = new FileReader(sciezka1);
        FileReader fileReader2 = new FileReader(sciezka2);
        BufferedReader bufferedReader1 = new BufferedReader(fileReader1);
        BufferedReader bufferedReader2 = new BufferedReader(fileReader2);
        String liniaPliku1 = bufferedReader1.readLine();
        String liniaPliku2 = bufferedReader2.readLine();
        while (liniaPliku1 != null && liniaPliku2 != null) {
            if (!liniaPliku1.equals(liniaPliku2)) {
                return false;
            }
            liniaPliku1 = bufferedReader1.readLine();
            liniaPliku2 = bufferedReader2.readLine();
        }
        return liniaPliku1 == null && liniaPliku2 == null;
    }
}
