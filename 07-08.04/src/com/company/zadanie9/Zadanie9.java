package com.company.zadanie9;
/*
Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku, a następnie wyświetla liczbę linii oraz
liczbę znaków w pliku (edited)
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Zadanie9 {
    public static void main(String[] args) {
        try {
            sciezkaDoPliku("src\\com\\company\\test.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void sciezkaDoPliku(String plik) throws IOException {
        FileReader fileReader = new FileReader(plik);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = bufferedReader.readLine();
        int ileLini = 0;
        int ileZnakow =0;
        for (; line != null;ileLini++) {
            ileZnakow+= line.length();
            line=bufferedReader.readLine();
        }
        System.out.println(ileLini);
        System.out.println(ileZnakow);
    }
}
