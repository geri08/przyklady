package com.company.zadanie16;
/*
*ZADANIE #9*
Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku, a następnie zwraca mapę gdzie kluczem jest
litera a wartością liczba jej wystąpień.
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
        String sciezka = "zadanie8.txt";
        try {
            wyswietlmapie(stworzMape(sciezka));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void wyswietlmapie(Map<String, Integer> stringIntegerMap) {
        for (Map.Entry<String ,Integer> element : stringIntegerMap.entrySet()) {
            System.out.printf("znak wystepuje: [%s] wystepuje %s razy \n ",element.getKey(), element.getValue());
        }
    }

    private static Map<String, Integer> stworzMape(String sciezka) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(sciezka);
        Map<String, Integer> nowaMapa = new TreeMap<>();
        int znak = fileInputStream.read();
        while (znak != -1) {
            String nowaLitera = String.valueOf((char) znak).toLowerCase();
            if (nowaMapa.containsKey(nowaLitera)) {
                int staraLiczbaWystapien = nowaMapa.get(nowaLitera);
                nowaMapa.put(nowaLitera, staraLiczbaWystapien + 1);
            } else {
                nowaMapa.put(nowaLitera, 1);
            }
            znak = fileInputStream.read();
        }

        return nowaMapa;
    }
}
