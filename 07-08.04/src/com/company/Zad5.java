package com.company;
/*
Utwórz metodę która przyjmuje niepustą liste lub tablicę a następnie w nieskończonej pętli je wyświetla
(zaczynając od pozycji `0`). Pętla ma zostać przerwana po wyjściu poza zakres listy (tzn. po przekroczeniu jej rozmiaru).
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Zad5 {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>();
        wypelnijListe(lista, 20);
        wyswietlListe(lista);
    }

    private static void wypelnijListe(List<Integer> lista, int rozmiarListy) {
        Random random = new Random();
        for (int i = 0; i < rozmiarListy; i++) {
            lista.add(random.nextInt(20));
        }
    }

    private static void wyswietlListe(List<Integer> lista) {

        try {
            for (int i = 0; ; i++) {
                System.out.print(lista.get(i) + ", ");
            }
        } catch (Exception wyjatek) {
        }
        System.out.print("\b\b");
    }
}
