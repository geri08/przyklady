package com.company.zadanie8;
/*

Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku, a następnie wyświetla zawartość tego pliku na konsolę.
Zrealizuj zadanie odczytując dane “znak po znaku” oraz “linia po linii”
 */

import java.io.*;

public class Main {
    public static void main(String[] args) {
        try {
            sciezkaDoPliku("src\\com\\company\\test.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println();
        try {
            wyswietlplik("src\\com\\company\\test.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static void wyswietlplik(String napis) throws IOException {
        FileReader fileReader = new FileReader(napis);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = bufferedReader.readLine();
        while (line != null) {
            System.out.println(line);
            line = bufferedReader.readLine();
        }
    }

    private static void sciezkaDoPliku(String plik) throws IOException {
        FileInputStream input = new FileInputStream(plik);
        int znak = input.read();
        while (znak != -1) {
            System.out.print((char) znak);
            znak = input.read();
        }
    }
}
