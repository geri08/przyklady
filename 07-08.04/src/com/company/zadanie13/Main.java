package com.company.zadanie13;
/*
*ZADANIE #6*
Utwórz metodę która przyjmuje dwa parametry,
które są ścieżkami do plików. Metoda ma scalić pliki “zazębiając” linijki
 */

import java.io.*;

public class Main {
    public static void main(String[] args) {
        String sciezka1 = "plik1.txt";
        String sciezka2 = "plik2.txt";
        try {
            wymieszak(sciezka1, sciezka2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void wymieszak(String sciezka1, String sciezka2) throws IOException {
        FileReader fr1 = new FileReader(sciezka1);
        FileReader fr2 = new FileReader(sciezka2);
        BufferedReader br1 = new BufferedReader(fr1);
        BufferedReader br2 = new BufferedReader(fr2);
        FileWriter fr = new FileWriter("plik3.txt");
        BufferedWriter br3 = new BufferedWriter(fr);

        while (true) {
            String linia1 = br1.readLine();
            String linia2 = br2.readLine();
            if (linia1 == null && linia2 == null) {
                break;
            }
            if (linia1 != null) {
                br3.write(linia1);
                br3.newLine();
            }
            if (linia2 != null) {
                br3.write(linia2);
                br3.newLine();
            }

        }
        br1.close();
        br2.close();
        br3.close();
    }
}
