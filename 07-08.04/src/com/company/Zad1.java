package com.company;
/*
Utwórz metodę która przyjmuje jeden parametr (typu `int`) a następnie liczy pierwiastek z podanej licby
(użyj `Math.sqrt()`). Jeśli podany parmetr będzie liczbą ujemną rzuć wyjątek (np. `IllegalArgumentException`)
 z komunikatem błędu. (edited)
 */

public class Zad1 {
    public static void main(String[] args) {
        double liczba = 40;
        try {
            double wynik2 = pierwiastekZWyjatkiem(liczba);
            System.out.println(wynik2);
        } catch (IllegalArgumentException wyjatek) {
            System.out.println("podałeś ujemna liczbę ");
        }

    }

    private static double pierwiastekZWyjatkiem(double liczba) {
        if (liczba < 0) {
            throw new IllegalArgumentException("zjebałeś ;( ");
        }

        return Math.sqrt(liczba);
    }
}
