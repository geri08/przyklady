package com.company;
/*
Utwórz metodę która przyjmuje jako parametr datę (w formie Stringa) w postaci `2017-03-24` (tj. `rok-miesiac-dzień`)
oraz zwróć datę w postaci `24.03.2017` (tj. `dzień.miesiąc.rok`)
 */

import java.time.DateTimeException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Zad8 {
    public static void main(String[] args) {
//        System.out.println(poprawDate( "2018-04-07"));
        System.out.println(ileDniZyje("1987-07-15"));
    }

    private static String ileDniZyje(String dataPoczatkowa) {
        LocalDate dataUrodzenia = LocalDate.parse(dataPoczatkowa);
        LocalDate obecnaData = LocalDate.now();
        System.out.println(dataUrodzenia);
        System.out.println(obecnaData);
//        Period period = Period.between(obecnaData,dataUrodzenia);
        Duration duration = Duration.between(obecnaData,dataUrodzenia);
        return duration.toString();
    }

    private static String poprawDate(String data) {
        LocalDate localDate = LocalDate.parse(data);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM!YYYY");
        return localDate.format(dateTimeFormatter);
    }
}
