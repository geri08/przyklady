package com.company.zadanie10;

public class Osoba {
    private String imie;
    private String nazwisko;
    private int wiek;
    private boolean czyKobieta;

    public Osoba(String imie, String nazwisko, int wiek, boolean czyKobieta) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.czyKobieta = czyKobieta;
    }

    public void wyswtietlDane() {
        System.out.printf("nazywam się  %s %s  mam %s lat oraz jestem %s \n"
                , imie, nazwisko, wiek, czyKobieta ? "kobietą" : "mężczyzną");
    }
}
