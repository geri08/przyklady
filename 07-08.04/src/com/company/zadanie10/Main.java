package com.company.zadanie10;
/*
Przygotuj plik .csv (gdzie wartości oddzielone są przecinkami - `,`), który będzie zawierał reprezentację
obiektów klasy `Osoba`. Metoda ma odczytać plik i zwrócić listę obiektów klasy `Osoba`.
```Jan,Nowak,12,false
Anna,Kowalska,25,true
Kuma,Janowski,66,false
Karolina,Nowa,88,true
...```
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Osoba> lisiaOsob = pobierzListeOsob("src\\com\\company\\osoby.txt");
        wyswietlListe(lisiaOsob);
    }

    private static void wyswietlListe(List<Osoba> lisiaOsob) {
        for (Osoba s : lisiaOsob) {
            s.wyswtietlDane();
        }
    }

    private static List<Osoba> pobierzListeOsob(String plik) {
        List<Osoba> listaosob = new ArrayList<>();
        try {
            FileReader fileReader = new FileReader(plik);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String linia = bufferedReader.readLine();
            while (linia != null) {
                listaosob.add(wczytajOsobe(linia));
                linia = bufferedReader.readLine();
            }
            return listaosob;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    private static Osoba wczytajOsobe(String linia) throws Exception {
        String[] tablica = linia.split(",");
        if (tablica.length != 4) {
            throw new Exception("błędne dane w pliku wejsciowym, liczba danych w wierszu " + tablica.length);
        }
        if(!tablica[3].equals("true") && !tablica[3].equals("false")){
            throw new IOException("podałeś błędne dane w płci, wpisane: " + tablica[3]);
        }
        return new Osoba(
                tablica[0],
                tablica[1],
                Integer.parseInt(tablica[2]),
                Boolean.parseBoolean(tablica[3]));
    }

}
