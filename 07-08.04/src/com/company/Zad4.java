package com.company;
/*
Utwórz metodę, która przyjmuje dwa parametry - liczbę (`int`) oraz napis (`String`). Metoda ma wyświetlić podany napis,
 przekazaną liczbę razy. W przypadku gdy liczba wystąpień będzie mniejsza bądź równa zero pownien zostać rzucony wyjątek
 `IndexOutOfBoundsException`, a w przypadku gdy napis będzie `null`-em pownien zostać rzucony wyjątek `IllegalArgumentException`. (edited)
 */

public class Zad4 {
    public static void main(String[] args) {
        int liczba = 5;
        String napis = "napis";
        try {
            metoda(liczba, null);
        }catch (IllegalArgumentException wyjatek){
            System.out.println(wyjatek.getMessage());
        }
    }

    private static void metoda(int krotnosc, String napis) {
        if (krotnosc <= 0) {
            throw new IllegalArgumentException("liczba powtorzen jest mniejsza badz rowna 0");
        } else if (napis == null) {
            throw new IllegalArgumentException("popsute");
        }

        for (int i = 0; i < krotnosc; i++) {
            System.out.println(napis);
        }
    }
}
