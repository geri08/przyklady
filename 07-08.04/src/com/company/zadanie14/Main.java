package com.company.zadanie14;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
*ZADAENIE *7*

Utwórz metodę która pobiera od użytkownika login i hasło, a następnie sprawdza czy podane dane
 są prawidłowe (weryfikując to z plikiem, w którym wartości rozdzielone są spacją)
admin admin123
mojLogin 123456
 */
public class Main {
    public static String userName;
    public static String haslo;

    public static void main(String[] args) {
        String sciezka = "dostep.txt";
        Pair<String, String> pobranaLista = pobierzDane();
        try {
            List<Pair<String, String>> nowaLista = zwrocListe(sciezka);
            boolean wynik =porownaj(pobranaLista, nowaLista);
            System.out.println(wynik);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean porownaj(Pair<String, String> pobranaLista, List<Pair<String, String>> nowaLista) {
        for (Pair<String, String> pozycjaListy : nowaLista) {
            if (pozycjaListy.equals(pobranaLista)) {
                return true;
            }
        }
        return false;
    }

    private static List<Pair<String, String>> zwrocListe(String sciezka) throws IOException {
        FileReader fileReader = new FileReader(sciezka);
        BufferedReader br = new BufferedReader(fileReader);
        List<Pair<String, String>> lista = new ArrayList<>();

        String linia = br.readLine();
        while (linia != null) {
            Pair<String, String> para = stworzPare(linia);
            lista.add(para);
            linia = br.readLine();
        }
        return lista;
    }

    private static void wyswietlListe(List<Pair<String, String>> lista) {
        for (Pair<String, String> stringStringPair : lista) {
            System.out.println(stringStringPair + " {}");
        }
    }

    private static Pair<String, String> stworzPare(String linia) {
        String[] tablica = linia.split(" ");
        return new Pair<String, String>(tablica[0], tablica[1]);
    }


    private static Pair<String, String> pobierzDane() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj login:");
        userName = scanner.nextLine();
        System.out.print("podaj haslo: ");
        haslo = scanner.nextLine();
        return new Pair<>(userName, haslo);
    }
}
