package com.company;
/*
Utwórz metodę która przyjmuje jako parametr datę (w formie Stringa) w postaci `24-03-2017` lub `24.03.2017`
oraz ma zwrócić informację który to jest dzień roku.
 */

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Zad9 {
    public static void main(String[] args) {
        System.out.println(ktoryToDzienRoku("07-04-2018"));
    }

    private static int ktoryToDzienRoku(String data) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate dzisiejszadata = LocalDate.parse(data,dateTimeFormatter);
        return dzisiejszadata.getDayOfYear();
//        LocalDate localDate = LocalDate.parse(data);
//        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-YYYY");
//        localDate.format(dateTimeFormatter);
//        return localDate.getDayOfYear();

    }
}
