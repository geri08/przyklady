package com.company;
/*
Utwórz metodę która wczytuje liczbę (czyli typ `int`) od użytkownika a następnie ją wyświetla. W przypadku podania
innej wartości (np. litery), wyświetl użytkownikowi komunikat i pobierz informację ponownie. Zaimplementuj to na dwa
sposoby (pierwszy - obsługa wyjątku przez metodę pobierającą, drugi - metoda rzucająca wyjątek)
 */

import java.lang.ref.SoftReference;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Zad6 {
    public static void main(String[] args) {
//        wczytajLiczbe();
        try {
            System.out.println(wczytajLiczbe2());
        }catch (InputMismatchException e){
            System.out.println("zjebałeś :/");
        }
    }

    private static int wczytajLiczbe2() throws InputMismatchException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podajj liczbe: ");

        return scanner.nextInt();
    }

    private static void wczytajLiczbe() {
        Scanner scanner = new Scanner(System.in);
        int liczba;
        while (true) {
            try {
                System.out.print("podaj liczbę: ");
                liczba = scanner.nextInt();
                System.out.println(liczba);
                break;
            } catch (InputMismatchException wyjatek) {
                System.out.println("nie podałeś liczby ;/");
            } finally {
                scanner.nextLine();
            }
        }

    }
}
