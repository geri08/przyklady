package com.company;

/*
Utwórz metodę, która przyjmuje parametry typu `int` korzystając z mechanizmu `varargs`. W przypadku nie przekazania
żadnego parametru, metoda powinna rzucić własny wyjątek (z komunikatem o powodzie). W przypadku podania parametrów,
 metoda powinna zwrócić ich sumę.
 */
public class Zad3 {
    public static void main(String[] args) {

        try {
            System.out.println(metoda());
        } catch (WlasnyWyjatek wlasnyWyjatek) {
            System.out.println(wlasnyWyjatek.getMessage());
            wlasnyWyjatek.getStackTrace();
        }
    }

    private static int metoda(int... liczby) throws WlasnyWyjatek {
        if (liczby.length == 0) {
            throw new WlasnyWyjatek("brak podanych liczb");
        }
        int suma = 0;
        for (int i : liczby) {
            suma += i;
        }
        return suma;
    }
}

class WlasnyWyjatek extends Exception {

    public WlasnyWyjatek(String message) {
        super(message);
    }
}
