package com.company;
/*
*ZADANIE #1*
Utwórz metodę, która przyjmuje listę (`List`) np. `ArrayList` elementów
typu `String` a następnie zwraca listę w odwróconej kolejności.
 */

import java.util.ArrayList;
import java.util.List;

public class Zad1 {

    public static void main(String[] args) {
        List <String> mojaLista = new ArrayList<String>();
        mojaLista.add("pierwszy");
        mojaLista.add("drugi");
        mojaLista.add("trzeci");
        mojaLista.add("czwarty");
        mojaLista.add("piaty");
        mojaLista.add("szosty");

        odwrocListe(mojaLista);
        System.out.println(mojaLista);

    }

    private static void odwrocListe(List<String> mojaLista) {

        for (int pozycja = 0; pozycja < mojaLista.size()/2; pozycja++) {
            int index = (mojaLista.size()-1-pozycja);
            String tymczasowa = mojaLista.get(pozycja);
            mojaLista.add(pozycja,mojaLista.get(index));
            mojaLista.add(index,tymczasowa);
        }


        for (int i = 0; i < mojaLista.size(); i++) {
            System.out.println("aa");
//            System.out.print(mojaLista.get(i)+ ", ");
        }
        System.out.println(mojaLista);
    }
}
