package com.company;
/*
Given a rectangular matrix of characters, add a border of asterisks(*) to it.

Example

For

picture = ["abc",
           "ded"]
the output should be

addBorder(picture) = ["*****",
                      "*abc*",
                      "*ded*",
                      "*****"]
 */

public class AddBorder {
    public static void main(String[] args) {
        String[] picture = new String[]{"2a", "2d"};
        addBorder(picture);
    }

    public static String[] addBorder(String[] picture) {
        String[] tablica = new String[picture.length + 2];
        tablica[0] = "*";
        tablica[tablica.length - 1] = "*";
        for (int i = 0; i < picture.length; i++) {
            tablica[i+1]= '*'+picture[i]+'*';
        }
        tablica[0]=tablica[tablica.length-1]=tablica[1].replaceAll(".","*");

        wyswietlTablica(tablica);
        return tablica;
    }

    private static void wyswietlTablica(String[] tablica) {
        for (String s : tablica) {
            System.out.println(s + ", ");
        }
    }
}