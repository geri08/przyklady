package com.company;
/*
Given a sorted array of integers a, find an integer x from a such that the value of

abs(a[0] - x) + abs(a[1] - x) + ... + abs(a[a.length - 1] - x)
is the smallest possible (here abs denotes the absolute value).
If there are several possible answers, output the smallest one.

Example

For a = [2, 4, 7], the output should be
absoluteValuesSumMinimization(a) = 4.
 */

public class AbsoluteValuesSumMinimization {
    public static void main(String[] args) {
        int [] tablica = new int[]{ -100000, -10000,-1000, -100, -10, -1, 0, 1, 10, 100, 1000,10000, 100000};
        System.out.println(absoluteValuesSumMinimization(tablica));
    }

    private static int absoluteValuesSumMinimization(int[] tablica) {
        int nowa [] = new int[tablica.length];
        int najmniejsza =1000000;
        int indeks =0;
        for (int i = 0; i < nowa.length; i++) {
            for (int j = 0; j < tablica.length; j++) {
                nowa[i] += Math.abs(tablica[j] -tablica[i] );
                System.out.print(nowa[i] + ", ");
            }
            System.out.println();
            if(nowa[i]<najmniejsza){
                najmniejsza =nowa[i];
                indeks =i;
            }
        }
//        wyswietlTablice(tablica);
//        wyswietlTablice(nowa);
//        System.out.println(najmniejsza);
//        System.out.println(indeks);
        return tablica[indeks];
    }

    private static void wyswietlTablice(int[] tablica) {
        for (int i : tablica) {
            System.out.print(i+"\t");
        }
        System.out.println();
    }
}
