package com.company;
/*
You are given an array of integers representing coordinates of obstacles situated on a straight line.

Assume that you are jumping from the point with coordinate 0 to the right. You are allowed only to
make jumps of the same length represented by some integer.

Find the minimal length of the jump enough to avoid all the obstacles.

Example

For inputArray = [5, 3, 6, 7, 9], the output should be
avoidObstacles(inputArray) = 4.
 */

public class AvoidObstacles {
    public static void main(String[] args) {
        int[] tablica = new int[]{2,3};
        System.out.println(avoidObstacles(tablica));
    }

    private static int avoidObstacles(int[] tablica) {
        int najwieksza = tablica[0];
        int najmniejsza = tablica[0];
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] > najwieksza) {
                najwieksza = tablica[i];
            }
            if (tablica[i] < najmniejsza) {
                najmniejsza = tablica[i];
            }
        }
        System.out.println(najmniejsza+", "+najwieksza);
        int skok = najmniejsza;
        int jump =skok;
        for (; jump <= najwieksza; ) {
            System.out.println("a " + skok +" ." + jump);
            if(czyWystepuje(tablica, jump)){
//                System.out.println(skok);
                jump+= skok;
            }else{
//                System.out.println("y");
                skok++;
                jump=skok;
            }
            System.out.println("b " + skok +" ." + jump);
        }
        return skok;
    }

    private static boolean czyWystepuje(int[] tablica, int skok) {
        for (int j = 0; j < tablica.length; j++) {
            if (skok == tablica[j]) {
                System.out.println(skok +":"+tablica[j]);
                return false;
            }
        }
        return true;
    }
}
