package com.company;
/*
Simple, given a string of words, return the length of the shortest word(s).
String will never be empty and you do not need to account for different data types.
 assertEquals(3, Kata.findShort("bitcoin take over the world maybe who knows perhaps"));
   assertEquals(3, Kata.findShort("turns out random test cases are easier than writing out basic ones"));

 */

public class NajkrotszeSlowo {
    public static void main(String[] args) {
        String napis = "bitcoin take over the world maybe who knows perhaps ";
        System.out.println(sprawdzNajkrotszyWyraz(napis));
    }

    private static int sprawdzNajkrotszyWyraz(String s) {
        int dlugoscwyrazu = s.length();
        int licznik = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                System.out.print(dlugoscwyrazu +", " + licznik+"..\n" );
                if (licznik <= dlugoscwyrazu && licznik>0) {
                    dlugoscwyrazu = licznik;
                    System.out.print(dlugoscwyrazu +", ");
                }
                licznik = 0;
            } else {
                licznik++;
            }
        }
        return dlugoscwyrazu;
    }
}
