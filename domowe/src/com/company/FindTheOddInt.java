package com.company;
/*
Given an array, find the int that appears an odd number of times.
There will always be only one integer that appears an odd number of times.
   	assertEquals(5, FindOdd.findIt(new int[]{20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5}));
    assertEquals(-1, FindOdd.findIt(new int[]{1,1,2,-2,5,2,4,4,-1,-2,5}));
  	assertEquals(5, FindOdd.findIt(new int[]{20,1,1,2,2,3,3,5,5,4,20,4,5}));
   	assertEquals(10, FindOdd.findIt(new int[]{10}));
   	assertEquals(10, FindOdd.findIt(new int[]{1,1,1,1,1,1,10,1,1,1,1}));
    assertEquals(1, FindOdd.findIt(new int[]{5,4,3,2,1,5,4,3,2,10,10}));
 */

public class FindTheOddInt {
    public static void main(String[] args) {
        int A[] = new int[]{20, 1, -1, 2, -2, 3, 3, 5, 5, 1, 2, 4, 20, 4, -1, -2, 5};
        System.out.println(findIt(A));
    }

    public static int findIt(int[] A) {
        for (int i = 0; i < A.length; i++) {
            int licznik = 0;
            for (int j = 0; j < A.length; j++) {
                if (A[i] == A[j]) {
                    licznik++;
                }
            }
            if (licznik %2 !=0){
                return A[i];
            }
        }
        return -1;
    }
}
