package com.company;
/*
Given a string, find out if its characters can be rearranged to form a palindrome.

Example

For inputString = "aabb", the output should be
palindromeRearranging(inputString) = true.

We can rearrange "aabb" to make "abba", which is a palindrome.
 */

import java.util.ArrayList;
import java.util.List;

public class Palindrome {
    public static void main(String[] args) {
        String napis = "aabb";
        System.out.println(palindromeRearranging(napis));
    }

    private static boolean palindromeRearranging(String napis) {

        int[] niz = new int[26];

        for (int i = 0; i < napis.length(); i++) {
            System.out.print(niz[0] + ".   ");
            niz[napis.charAt(i) - 97]++;
            System.out.println(niz[niz[napis.charAt(i) - 97]]);
        }
        for (int i : niz) {
            System.out.print(i + ". ");
        }
        int cnt = 0;
        for (int i = 0; i < niz.length; i++)
            if (niz[i] % 2 != 0)
                cnt++;

        return cnt <= 1;

    }

}
