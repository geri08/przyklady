package com.company;
/*
Given an array of equal-length strings, check if it is possible to rearrange the strings in such a way that after the
rearrangement the strings at consecutive positions would differ by exactly one character.

Example

For inputArray = ["aba", "bbb", "bab"], the output should be
stringsRearrangement(inputArray) = false;

All rearrangements don't satisfy the description condition.

For inputArray = ["ab", "bb", "aa"], the output should be
stringsRearrangement(inputArray) = true.

Strings can be rearranged in the following way: "aa", "ab", "bb".
Input/Output
 */

public class StringsRearrangement {
    public static void main(String[] args) {
        String[] inputArrays = new String[]{"abc", "abx", "axx", "abx", "abc"};   //true


//        System.out.println(stringsRearrangement(inputArrays));
    }
}


        /*
        int licznik = 0;
        for (int i = 0; i < inputArrays.length; i++) {
            int a = 0;
            for (int j = 0; j < inputArrays.length; j++) {
                // System.out.printf("( i=%s, j=%s ) %s : %s = %s\n",i,j,inputArrays[i],inputArrays[j],sprawdzCzyPodobne(inputArrays[i],inputArrays[j]));
                if (sprawdzCzyPodobne(inputArrays[i], inputArrays[j]) == 1) {
                    licznik++;
                    a++;
                }
                if (inputArrays[i].equals(inputArrays[j]) && i != j) {
                    licznik -= 1;
                }
                if (a == 2) {
                    break;
                }


            }
            System.out.println(licznik + " i; " +i);
        }
        return (inputArrays.length - 1) * 2 == licznik;
    }

    private static int sprawdzCzyPodobne(String pierwszy, String drugi) {

        int ilerazy = 0;

        for (int i = 0; i < pierwszy.length(); i++) {
            if (pierwszy.charAt(i) != drugi.charAt(i)) {
                ilerazy++;
            }

        }
        return ilerazy;
    }
}
*/
/*
// if(inputArrays[0].length()==1){
      //       return true;
      //   }
 int licznik = 0;
        for (int i = 0; i < inputArrays.length; i++) {
            int a = 0;
            for (int j = 0; j < inputArrays.length; j++) {
//                System.out.printf("%s : %s = %s\n",inputArrays[i],inputArrays[j],sprawdzCzyPodobne(inputArrays[i],inputArrays[j]));
                if (sprawdzCzyPodobne(inputArrays[i], inputArrays[j]) == 1) {
                    licznik++;
                    a++;
                }
                if (a == 2) {
                    break;
                }

            }
        }
        System.out.println(licznik);
        return (inputArrays.length - 1) * 2 == licznik;
    }

     int sprawdzCzyPodobne(String pierwszy, String drugi) {
        int ilerazy = 0;
        for (int i = 0; i < pierwszy.length(); i++) {
            if (pierwszy.charAt(i) != drugi.charAt(i)) {
                ilerazy++;
            }

        }
        return ilerazy;
    }


 */