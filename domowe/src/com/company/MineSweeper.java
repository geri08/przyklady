package com.company;

/*
In the popular Minesweeper game you have a board with some mines and those cells that don't contain a mine
have a number in it that indicates the total number of mines in the neighboring cells. Starting off with some
arrangement of mines we want to create a Minesweeper game setup.

Example

For

matrix = [[true, false, false],
          [false, true, false],
          [false, false, false]]
the output should be

minesweeper(matrix) = [[1, 2, 1],
                       [2, 1, 1],
                       [1, 1, 1]]
Check out the image below for better understanding:

 */
public class MineSweeper {
    public static void main(String[] args) {
        boolean[][] plansza = new boolean[5][];
        plansza[0] = new boolean[]{true, false, false, true, false};
        plansza[1] = new boolean[]{false, true, false, false, false};
        plansza[2] = new boolean[]{false, false, false, true, true};
        plansza[3] = new boolean[]{false, false, false, false, false};
        plansza[4] = new boolean[]{false, false, false, true, false};
        mineSweeper(plansza);
    }

    private static int[][] mineSweeper(boolean[][] plansza) {
        int[][] uzupelniona = new int[plansza.length][plansza[0].length];
        for (int i = 0; i < uzupelniona.length; i++) {
            for (int j = 0; j < uzupelniona[0].length; j++) {
                if (plansza[i][j]) {
                    uzupelniona[i][j] = -1;
                } else {
                    uzupelniona[i][j] = 0;
                }
                uzupelniona[i][j] += sprawdzDoOkola(plansza, i, j);
            }
        }
        return uzupelniona;
    }

    private static int sprawdzDoOkola(boolean[][] plansza, int x, int y) {
        int licznik = 0;
        for (int i = x - 1; i < x + 2; i++) {
            for (int j = y - 1; j < y + 2; j++) {
                if (i>=0 && i<plansza.length && j>=0 && j<plansza[0].length && plansza[i][j] ) {
                    licznik++;
                }
            }
        }
        return licznik;
    }

    private static void wyswietlPlansze(int[][] plansza) {

        for (int i = 0; i < plansza.length; i++) {
            for (int j = 0; j < plansza[0].length; j++) {
                System.out.print(plansza[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
