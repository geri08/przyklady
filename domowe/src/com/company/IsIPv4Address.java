package com.company;
/*
An IP address is a numerical label assigned to each device (e.g., computer, printer) participating in a
computer network that uses the Internet Protocol for communication. There are two versions of the Internet protocol,
and thus two versions of addresses. One of them is the IPv4 address.

IPv4 addresses are represented in dot-decimal notation, which consists of four decimal numbers, each ranging
 from 0 to 255 inclusive, separated by dots, e.g., 172.16.254.1.

Given a string, find out if it satisfies the IPv4 address naming rules.

Example

For inputString = "172.16.254.1", the output should be
isIPv4Address(inputString) = true;

For inputString = "172.316.254.1", the output should be
isIPv4Address(inputString) = false.

316 is not in range [0, 255].

For inputString = ".254.255.0", the output should be
isIPv4Address(inputString) = false.

There is no first number.
 */

public class IsIPv4Address {
    public static void main(String[] args) {
        String adresIP = "72.8.37.28";
        System.out.println(isIPv4Address(adresIP));
    }

    private static boolean isIPv4Address(String adresIP) {
        String wzorzec = "([0-9]{1,3}\\.){3}([0-9]{1,3})";
        if (adresIP.matches(wzorzec)) {
            String[] tablica = adresIP.split("\\.");
            int[] tab = new int[tablica.length];
            for (int i = 0; i < tab.length; i++) {
                tab[i] = Integer.parseInt(tablica[i]);
                if (tab[i] > 255 || tab[i] < 0 || tab.length == 3) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }
}
