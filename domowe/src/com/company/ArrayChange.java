package com.company;
/*
You are given an array of integers. On each move you are allowed to increase exactly one of its element by one.
 Find the minimal number of moves required to obtain a strictly increasing sequence from the input.

Example

For inputArray = [1, 1, 1], the output should be
arrayChange(inputArray) = 3
 */

public class ArrayChange {
    public static void main(String[] args) {
        int[] array = new int[]{1, 1, 1};
        System.out.println(arrayChange(array));
    }

    private static int arrayChange(int[] array) {
        int licznik = 0;
        for (int i = 0; i < array.length - 1; i++) {
            for (; ; ) {
                if (array[i] >= array[i + 1]) {
                    array[i + 1]+=1;
                    licznik++;
                } else {
                    break;
                }

            }
        }
        return licznik;
    }
}
