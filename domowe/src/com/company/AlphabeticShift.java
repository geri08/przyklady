package com.company;
/*
Given a string, replace each its character by the next one in the English alphabet (z would be replaced by a).

Example

For inputString = "crazy", the output should be
alphabeticShift(inputString) = "dsbaz".
 */

public class AlphabeticShift {
    public static void main(String[] args) {
        String napis = "crazy";
        alphabeticShift(napis);
    }

    private static String alphabeticShift(String napis) {
        String nowynapis = "";
        for (int i = 0; i < napis.length(); i++) {
            if (Integer.valueOf(napis.charAt(i)) == 122) {
                nowynapis += 'a';
            } else {
                nowynapis += (char) (Integer.valueOf(napis.charAt(i)) + 1);
            }
        }
        napis.charAt(1);
        return nowynapis;
    }
}
