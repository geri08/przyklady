package com.company;

public class Main {
    public static void main(String[] args) {
        int [] a = new int[]{50,60,30,10,12};
        System.out.println(alternatingSums(a));
    }
    public static int[] alternatingSums(int[] a) {
        int []tablica = new int[]{0,0};
        if(a.length>1){
            for(int i=1; i<a.length;i+=2){
                tablica[0]+=a[i-1];
                tablica[1]+=a[i];
            }
        }else{
            tablica[0]=a[0];
            tablica[1]=0;
        }
        for (int i : tablica) {
            System.out.println();
        }
        return tablica;
    }

}
