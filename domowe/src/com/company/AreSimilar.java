package com.company;
/*
Two arrays are called similar if one can be obtained from another by swapping at most one pair of elements in one of the arrays.

Given two arrays a and b, check whether they are similar.

Example

For a = [1, 2, 3] and b = [1, 2, 3], the output should be
areSimilar(a, b) = true.

The arrays are equal, no need to swap any elements.

For a = [1, 2, 3] and b = [2, 1, 3], the output should be
areSimilar(a, b) = true.

We can obtain b from a by swapping 2 and 1 in b.

For a = [1, 2, 2] and b = [2, 1, 1], the output should be
areSimilar(a, b) = false.

Any swap of any two elements either in a or in b won't make a and b equal.
 */

import java.util.ArrayList;
import java.util.List;

public class AreSimilar {
    public static void main(String[] args) {
        int[] a = new int[]{832, 998, 148, 570, 533, 561, 894, 147, 455, 279};
        int[] b = new int[]{832, 998, 148, 570, 533, 561, 455, 147, 894, 279};
        System.out.println(areSimilar(a, b));
    }

    private static boolean areSimilar(int[] a, int[] b) {
        List<Integer> bledne = new ArrayList<>();
        int licznik = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                System.out.println("x");
                licznik++;
                bledne.add(a[i]);
                bledne.add(b[i]);
            }
            if (licznik > 2) {

                return false;
            }
        }
        if (licznik == 2) {
            System.out.println("y");
            if (!bledne.get(0).equals(bledne.get(3)) || !bledne.get(1).equals(bledne.get(2))) {
                return false;
            }
        }
        return true;
//        System.out.println(bledne.get(0)+(bledne.get(3))+bledne.get(1)+(bledne.get(2)));
    }
}
