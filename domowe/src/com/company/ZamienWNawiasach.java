package com.company;

import java.util.ArrayList;
import java.util.List;

public class ZamienWNawiasach {
    public static void main(String[] args) {
        String s = "abc(cba)ab(bac)c";
        System.out.println(reverseParentheses(s));
    }

    private static String reverseParentheses(String s) {
        List<Character> lista = zamienNapisNaListe(s);
        zamienWnawiasach(lista);
        String napis = zamienNanapis(lista);
        System.out.println(napis);
        return napis;
    }

    private static String zamienNanapis(List<Character> lista) {
       String napis = "";
        for (Character character : lista) {
            napis+=character;
        }
        return napis;
    }

    private static void zamienWnawiasach(List<Character> lista) {
        List<Integer> leweNawiasy = znajdzIndexyNawiasow(lista, '(');
        char przechwyt;
        for (int i = 0; i < leweNawiasy.size(); i++) {
            System.out.println(lista);
            int indekslewyNawias = znajdzLewy(lista);
            int indeksPrawyNawias = znajdzPrawy(lista,indekslewyNawias);
            System.out.println(indekslewyNawias+", "+ indeksPrawyNawias);
            for (int j = 0; j < (indeksPrawyNawias - indekslewyNawias - 1) / 2; j++) {
                przechwyt = lista.get(indekslewyNawias + j + 1);
                lista.set(indekslewyNawias + j + 1, lista.get(indeksPrawyNawias - 1 - j));
                lista.set(indeksPrawyNawias - 1 - j, przechwyt);
            }
            usunNawiasy(lista, indekslewyNawias, indeksPrawyNawias);
            System.out.println(lista);
        }
    }

    private static void usunNawiasy(List<Character> lista, int leweNawiasy, int praweNawiasy) {
        lista.remove(praweNawiasy);
        lista.remove(leweNawiasy);
    }

    private static int znajdzPrawy(List<Character> lista, int lewynawias) {
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) == ')') {
                if(lewynawias<i){
                    return i;
                }
            }
        }
        return -1;
    }

    private static int znajdzLewy(List<Character> lista) {
        for (int i = lista.size() -1; i >= 0; i--) {
            if (lista.get(i) == '(') {
                return i;
            }
        }
        return -1;
    }

    private static List<Integer> znajdzIndexyNawiasow(List<Character> lista, char nawias) {
        List<Integer> indeksyNawiasow = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) == nawias) {
                indeksyNawiasow.add(i);
            }
        }
        return indeksyNawiasow;
    }

    private static List<Character> zamienNapisNaListe(String s) {
        List<Character> list = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            list.add(i, s.charAt(i));
        }
        return list;
    }
}
