package com.company.Zadanie3;

import com.company.Test.Apple;
import com.company.Zadanie2.Circle;
import com.company.Zadanie2.KsztaltBox;
import com.company.Zadanie2.Square;

public class Main {
    public static void main(String[] args) {
        MetodyGeneryczne jeden = new MetodyGeneryczne();
        KsztaltBox<Circle> kolo1 = new KsztaltBox<>(new Circle());
        KsztaltBox<Square> kwadrat1 = new KsztaltBox<>(new Square());

//        jeden.metodaJeden(kolo1);
//        jeden.metodaDwa(kwadrat1);
//        jeden.metodaDwa(kolo1);
//        jeden.metodaTrzy(kolo1);
//        jeden.metodaTrzy(kwadrat1);
//        jeden.metodaPiata(new Circle());
//        jeden.metodaPiata("ide se");
//        jeden.metodaPiata(222);
//        jeden.metodaPiata(new Square());
//        jeden.metodaSzczesc(new Circle());
//        String napis = jeden.metodaSzczesc("mmmm");
//        System.out.println(napis);
//        jeden.metodaSiedem(new Square());
//        jeden.metodaSiedem(new Circle());
        jeden.metodaOsiem(new Circle(),new Apple());
        jeden.metodaOsiem(new Square(),new Apple());
    }
}
