package com.company.Zadanie3;

import com.company.Test.Apple;
import com.company.Zadanie2.Circle;
import com.company.Zadanie2.Ksztalt;
import com.company.Zadanie2.KsztaltBox;

import java.util.List;

public class MetodyGeneryczne {

    void metodaJeden(KsztaltBox<Circle> ksztaltBox) {
        System.out.println("przekazanie obiekt klasy Circle");
    }

    void metodaDwa(KsztaltBox<?> obiekt) {
        System.out.println("przekazano obiekt klasy " + obiekt.getPojemnik().getClass().getSimpleName());
    }

    void metodaTrzy(KsztaltBox<? extends Ksztalt> ksztaltBox) {
        System.out.println("przekazano obiekt klasy " + ksztaltBox.getPojemnik().getClass().getSimpleName());
    }

    void metodaCztery(List<? super Ksztalt> listaKsztalt) {

    }

    <T> void metodaPiata(T parametr) {
        System.out.println("przekazano obiekt klasy " + parametr.getClass().getSimpleName());

    }

    <T> T metodaSzczesc(T parametr) {
        System.out.println("nazwa klasy: " + parametr.getClass().getSimpleName());
        return parametr;
    }

    <K extends Ksztalt> K metodaSiedem(K parametr) {
        parametr.hello();
        return parametr;
    }

    <P extends Ksztalt, W extends Apple> void metodaOsiem(P ksztalt, W apple) {
        ksztalt.hello();
        apple.wycisnijSok();
    }

}
