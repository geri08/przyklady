package com.company.Zadanie2;

public class Main {
    public static void main(String[] args) {
        KsztaltBox<Circle> ksztaltkola = new KsztaltBox<>(new Circle());
        System.out.println(ksztaltkola.getPojemnik().getClass().getSimpleName());

        KsztaltBox<Square> ksztaltkwadrat = new KsztaltBox<>(new Square());
        System.out.println(ksztaltkwadrat.getPojemnik().getClass().getSimpleName());
//  nie zadziała, ponieważ Integer nie implementuje
//        KsztaltBox<String> string = new KsztaltBox<String>("dsd");

    }
}
