package com.company.Zadanie2;

public class KsztaltBox<TYP extends Ksztalt> {
   private TYP pojemnik;

    public KsztaltBox(TYP pojemnik) {
        this.pojemnik = pojemnik;
    }

    public TYP getPojemnik() {
        return pojemnik;
    }

    public void setPojemnik(TYP pojemnik) {
        this.pojemnik = pojemnik;
    }
}
