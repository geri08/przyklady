package com.company.Zadanie5;

public class Osoba {
    private String imie;
    private int wiek;

    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    public String getImie() {
        return imie;
    }

    public int getWiek() {
        return wiek;
    }
    public void przedstawSie(){
        System.out.printf("czesc, jestem %s, oraz mam %s lat \n",imie,wiek);
    }
}
