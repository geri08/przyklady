package com.company.Zadanie5;

public class Main {
    public static void main(String[] args) {
//        przedstawSie();
//        komputer();
        mojKomputer();
    }

    private static void mojKomputer() {
    MojKomputer mojKomputer = new MojKomputer();
    mojKomputer.run();
    }

    private static void komputer() {
        Komputer komputer1 = new Komputer();
        komputer1.run();
        System.out.println();
        Komputer laptop = new Komputer() {
            @Override
            protected void przygotowanie() {
                System.out.println("ładowanie wszystkich komponentów");
                super.przygotowanie();
            }
        };
        laptop.run();

    }

    private static void przedstawSie() {
        Osoba osoba1 = new Osoba("adam", 20);
        Osoba osoba2 = new Osoba("alicja", 21);
        osoba1.przedstawSie();
        osoba2.przedstawSie();
        Osoba osoba3 = new Osoba("halina", 11) {
            @Override
            public void przedstawSie() {
                System.out.printf("siemanko, jestem %s i mam już %s wiosen\n", getImie(), getWiek());
            }
        };
        osoba3.przedstawSie();
        System.out.println(osoba1.getClass().getSimpleName());
        System.out.println(osoba3.getClass().getSimpleName());
        System.out.println(osoba3.getClass());
    }
}
