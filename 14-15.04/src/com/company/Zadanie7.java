package com.company;


import java.math.BigDecimal;

public class Zadanie7 {
    public static void main(String[] args) {
//        metoda1();
//        metoda2();
//        metoda3();
//        metoda4();
//        metoda5();
//        metoda6();
//        metoda7();
        metoda8();
    }

    private static void metoda8() {
        float a = 100_000_000_000f;
        double b = 100_000_000_000d;
        System.out.printf("%f\n", a);
        System.out.printf("%f\n", b);
    }

    private static void metoda7() {
        BigDecimal duza = new BigDecimal(new char[]{'0', '.', '1'});
        BigDecimal duza2 = new BigDecimal("0.7");
        System.out.println(duza);
        System.out.println(duza2);
        float f = duza.multiply(duza2).floatValue();
        System.out.printf("%f", f);
    }

    private static void metoda6() {
        float a = 0.1f;
        double d = 0.1;
        System.out.printf("wartość float: %.50f \n", a);
        System.out.printf("wartość double: %.50f \n", d);
        System.out.println(a + d);

        System.out.println(0.10000000149011612000000000000000000000000000000000 == 0.1f);
    }

    private static void metoda5() {
        System.out.println(3 / 0.0);
    }

    private static void metoda4() {
        double suma = 0;
        for (int i = 0; i < 10; i++) {
            suma += 0.1;
        }
        System.out.printf("%.2f", suma);
    }

    private static void metoda3() {
        for (float i = 10f; i != 0; i -= 0.1f) {
            System.out.println(i);
        }
    }

    private static void metoda2() {
        for (double i = 0; i < 1; i += 0.1d) {
            System.out.println(i);
        }
    }

    private static void metoda1() {
        for (float i = 0; i < 1; i += 0.1f) {
            System.out.println(i);
        }
    }
}
