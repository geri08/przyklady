package com.company.Zadanie4;

public class TrzyElementy<W, F, T> {
    private W pierwszy;
    private F drugi;
    private T trzeci;

    public TrzyElementy(W pierwszy, F drugi, T trzeci) {
        this.pierwszy = pierwszy;
        this.drugi = drugi;
        this.trzeci = trzeci;
    }

    public W getPierwszy() {
        return pierwszy;
    }

    public F getDrugi() {
        return drugi;
    }

    public T getTrzeci() {
        return trzeci;
    }
}
