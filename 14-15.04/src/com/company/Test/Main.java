package com.company.Test;

public class Main {
    public static void main(String[] args) {
//        bezGeneratora();
        generycznosc();
    }

    private static void generycznosc() {
        Basket<Orange> basket = new Basket<>(new Orange());
        Basket<String> basketString = new Basket<>(new String());
        System.out.println(basket.getFruit().getClass().getSimpleName());
        Orange pomarancz = new Orange();
        basket.setFruit(pomarancz);

        BiggerBasket<Apple, Orange> biggerBasket = new BiggerBasket<>(new Apple(), new Orange());
        System.out.println(biggerBasket.getPierwszy().getClass().getSimpleName());
        System.out.println(biggerBasket.getDrugi().getClass().getSimpleName());
        BiggerBasket<String, Integer> drugikKoszyk = new BiggerBasket<>("asssa", 3);
        System.out.println(drugikKoszyk.getPierwszy());
        System.out.println(drugikKoszyk.getDrugi());

    }

    private static void bezGeneratora() {
        Apple apple = new Apple();
        AppleBox appleBox = new AppleBox(apple);

        Orange orange = new Orange();
        OrangeBox orangeBox = new OrangeBox(orange);

        FruitBox jablkaBox = new FruitBox(apple);
        FruitBox pomaranczaBox = new FruitBox(orange);
        System.out.println(jablkaBox.getFruit().getClass().getSimpleName());
        System.out.println(pomaranczaBox.getFruit().getClass().getSimpleName());

        jablkaBox.setFruit(orange);
        System.out.println(jablkaBox.getFruit().getClass().getSimpleName());
        jablkaBox.setFruit("fffff");
        System.out.println(jablkaBox.getFruit().getClass().getSimpleName());
    }
}
