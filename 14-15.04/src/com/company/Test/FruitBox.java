package com.company.Test;

public class FruitBox {
    Object fruit;

    public FruitBox(Object fruit) {
        this.fruit = fruit;
    }

    public Object getFruit() {
        return fruit;
    }

    public void setFruit(Object fruit) {
        this.fruit = fruit;
    }
}
