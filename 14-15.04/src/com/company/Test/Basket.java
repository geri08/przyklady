package com.company.Test;

public class Basket <P> {
    P fruit;

    public Basket(P fruit) {
        this.fruit = fruit;
    }

    public P getFruit() {
        return fruit;
    }

    public void setFruit(P fruit) {
        this.fruit = fruit;
    }
}
