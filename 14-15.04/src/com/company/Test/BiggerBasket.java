package com.company.Test;

public class BiggerBasket<A,B> {
    A pierwszy;
    B drugi;

    public BiggerBasket(A pierwszy, B drugi) {
        this.pierwszy = pierwszy;
        this.drugi = drugi;
    }

    public A getPierwszy() {
        return pierwszy;
    }

    public B getDrugi() {
        return drugi;
    }
}
