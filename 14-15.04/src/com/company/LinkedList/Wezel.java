package com.company.LinkedList;

public class Wezel {
    private String dane;
    private Wezel nastepnyElement;

    public Wezel(String dane) {
        this.dane = dane;
    }

    public String getDane() {
        return dane;
    }

    public Wezel getNastepnyElement() {
        return nastepnyElement;
    }

    public void setNastepnyElement(Wezel nastepnyElement) {
        this.nastepnyElement = nastepnyElement;
    }

}
