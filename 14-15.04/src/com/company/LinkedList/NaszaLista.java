package com.company.LinkedList;

public class NaszaLista {
    private Wezel pierwszyElement;
    private int liczbaElementow;

    int size() {
        return liczbaElementow;
    }

    void dodajNaPoczatek(String dane) {
        Wezel nowywezel = new Wezel(dane);
        liczbaElementow++;
        if (pierwszyElement == null) {
            pierwszyElement = nowywezel;
        } else {
            nowywezel.setNastepnyElement(pierwszyElement);
            pierwszyElement = nowywezel;


        }

    }

    void add(String dane, int indeks) {
        if (indeks <= 0) {
            dodajNaPoczatek(dane);
            return;
        }
        if (indeks > liczbaElementow) {
            add(dane);
            System.out.println();
            System.out.println("przesadziłem z liczba indeksów, dodałem go na koniec");
            return;
        }
        liczbaElementow++;
        int obecnapozycja = 0;
        Wezel nowyWezel = new Wezel(dane);
        Wezel tymczasowy = pierwszyElement;
        while (obecnapozycja != indeks - 1) {
            obecnapozycja++;
            tymczasowy = tymczasowy.getNastepnyElement();
        }
        nowyWezel.setNastepnyElement(tymczasowy.getNastepnyElement());
        tymczasowy.setNastepnyElement(nowyWezel);

    }

    void zamienMiejscami(int pierwszy, int drugi) {
        if (pierwszy >= drugi) {
            System.out.println("wykonałem zadanie");
            return;
        }
        if (pierwszy < 0 || pierwszy > liczbaElementow - 1 || drugi < 0 || drugi > liczbaElementow - 1) {
            System.out.println("wyskoczyes poza zakres");
            return;
        }
        Wezel pierwszyWagonik = znajdzWagoni(pierwszy);
        Wezel przedPierwszyWagonik = znajdzWagoni(pierwszy - 1);
        Wezel drugiWagonik = znajdzWagoni(drugi);
        Wezel przedDrugiWagonik = znajdzWagoni(drugi - 1);
        Wezel tymczasowy = pierwszyWagonik.getNastepnyElement();
        if (drugi-pierwszy==1){
            przedPierwszyWagonik.setNastepnyElement(drugiWagonik);
            pierwszyWagonik.setNastepnyElement(drugiWagonik.getNastepnyElement());
            drugiWagonik.setNastepnyElement(pierwszyWagonik);
            return;
        }
        if (przedPierwszyWagonik == null) {
            pierwszyElement = drugiWagonik;
            tymczasowy = drugiWagonik.getNastepnyElement();
            drugiWagonik.setNastepnyElement(pierwszyWagonik.getNastepnyElement());
            przedDrugiWagonik.setNastepnyElement(pierwszyWagonik);
            pierwszyWagonik.setNastepnyElement(tymczasowy);
        } else {
            przedPierwszyWagonik.setNastepnyElement(drugiWagonik);
            przedDrugiWagonik.setNastepnyElement(pierwszyWagonik);
            pierwszyWagonik.setNastepnyElement(drugiWagonik.getNastepnyElement());
            drugiWagonik.setNastepnyElement(tymczasowy);

        }

//        if(przedPierwszyWagonik!=null){
//            pierwszyElement=pierwszyWagonik;
//        }
//        if(przedDrugiWagonik!=null){
//        }
//        if(drugiWagonik!=null){
//        }

    }

    private Wezel znajdzWagoni(int indeks) {
        if (indeks < 0 || indeks > liczbaElementow - 1) {
            return null;
        }
        int licznik = 0;
        Wezel tymczasowyWezel = pierwszyElement;
        while (licznik != indeks) {
            licznik++;
            tymczasowyWezel = tymczasowyWezel.getNastepnyElement();
        }
        return tymczasowyWezel;
    }

    void usunElement(int pozycjaDoUsuniecia) {
        int obecnaPozycja = 0;
        Wezel obecny = pierwszyElement;
        liczbaElementow--;
        if (pozycjaDoUsuniecia <= 0) {
            pierwszyElement = pierwszyElement.getNastepnyElement();
            return;
        }
        if (pozycjaDoUsuniecia > liczbaElementow) {
            pozycjaDoUsuniecia = liczbaElementow;
        }

        while (obecnaPozycja != pozycjaDoUsuniecia - 1) {
            obecnaPozycja++;
            obecny = obecny.getNastepnyElement();
        }
        obecny.setNastepnyElement(obecny.getNastepnyElement().getNastepnyElement());

    }

    void wyczyscListe() {
        pierwszyElement = null;
        liczbaElementow = 0;
    }

    void add(String dane) {
        liczbaElementow++;
        Wezel nowoDodanyWezel = new Wezel(dane);
        if (pierwszyElement == null) {
            pierwszyElement = nowoDodanyWezel;
        } else {
            Wezel kolejnyElement;
            kolejnyElement = pierwszyElement;
            for (; ; ) {
                if (kolejnyElement.getNastepnyElement() == null) {
                    break;
                }
                kolejnyElement = kolejnyElement.getNastepnyElement();
            }
            kolejnyElement.setNastepnyElement(nowoDodanyWezel);
        }
    }

    void wyswietlListe() {
        Wezel tymczasowy = pierwszyElement;
        while (true) {
            if (tymczasowy == null) {
                break;
            }
            System.out.printf("\" %s\" -> ", tymczasowy.getDane());
            tymczasowy = tymczasowy.getNastepnyElement();
        }
        System.out.print("\b\b\b\n");
    }
}