package com.company;

public class Zadanie8 {
    public static void main(String[] args) {
        int obrot = 100_000;

        metoda(obrot);
        metoda2(obrot);
        metoda3(obrot);
    }

    private static String metoda3(int obrot) {
        StringBuffer a = new StringBuffer();
        long start = System.currentTimeMillis();
        for (int i = 0; i < obrot; i++) {
            a.append("a");
        }
        String napis =a.toString();
        long end = System.currentTimeMillis();
        System.out.println("String buffer: " + (end - start));
        return napis;
    }

    private static String metoda2(int obrot) {
        StringBuilder a = new StringBuilder();
        long start = System.currentTimeMillis();
        for (int i = 0; i < obrot; i++) {
            a.append("a");
        }
        String napis = a.toString();
        long end = System.currentTimeMillis();
        System.out.println("StringBuilder: " + (end - start));
        return napis;
    }

    private static String metoda(int obrot) {
        String a = "";
        long start = System.currentTimeMillis();
        for (int i = 0; i < obrot; i++) {
            a += "a";
        }
        long stop = System.currentTimeMillis();

        System.out.println("zwykły += : " + (stop - start));
        return a;
    }
}
