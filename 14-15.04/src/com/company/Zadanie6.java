package com.company;
/*
rekurencja, wywołanie metody w metodzie
 */

public class Zadanie6 {
    public static void main(String[] args) {
        int silnia =liczenieIteracyjne(5);
        System.out.println(silnia);
        System.out.println(liczenieRekurencyje(5));
    }

    private static int liczenieRekurencyje(int liczba) {
        System.out.println("obecna wartosc liczby to: " + liczba);
        if (liczba==0){
            return 1;
        }
int wynik = liczenieRekurencyje(liczba-1)*liczba;
        System.out.println("wynik: " +wynik);
         return wynik;
    }

    private static int liczenieIteracyjne(int liczba) {
        int wynik = 1;
        for (int i = 1; i <=liczba; i++) {
            wynik*=i;
        }
        return wynik;
    }
}
