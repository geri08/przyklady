package com.company.zadanie9;

public class Zadaniecodefights {
    public static void main(String[] args) {
        System.out.println("poniższe wyniki powinny być TRUE");
        System.out.println(sprawdzTablice(new int[]{10, 1, 2, 3, 4, 5}));
        System.out.println(sprawdzTablice(new int[]{1, 3, 2}));
        System.out.println(sprawdzTablice(new int[]{1, 2, 3, 4, 5, 6}));
        System.out.println(sprawdzTablice(new int[]{0, -2, 5, 6}));
        System.out.println(sprawdzTablice(new int[]{1, 2, 5, 3, 5}));
        System.out.println(sprawdzTablice(new int[]{1, 2, 3, 4, 3, 6}));
        System.out.println(sprawdzTablice(new int[]{1, 2, 3, 4, 99, 5, 6}));
        System.out.println(sprawdzTablice(new int[]{123, -17, -5, 1, 2, 3, 12, 43, 45}));
        System.out.println(sprawdzTablice(new int[]{3, 5, 67, 98, 3}));
        System.out.println("\nponiższe wyniki powinny być FALSE");
        System.out.println(sprawdzTablice(new int[]{1, 3, 2, 1}));
        System.out.println(sprawdzTablice(new int[]{1, 2, 1, 2}));
        System.out.println(sprawdzTablice(new int[]{1, 3, 3, 3, 4}));
        System.out.println(sprawdzTablice(new int[]{40, 50, 60, 10, 20, 30}));
        System.out.println(sprawdzTablice(new int[]{1, 4, 10, 4, 2}));
        System.out.println(sprawdzTablice(new int[]{1, 1, 1, 2, 3}));
        System.out.println(sprawdzTablice(new int[]{1, 2, 3, 4, 5, 3, 5, 6}));
        System.out.println(sprawdzTablice(new int[]{1, 2, 5, 5, 5}));
        System.out.println(sprawdzTablice(new int[]{10, 1, 2, 3, 4, 5, 6, 1}));
    }

    private static boolean sprawdzTablice(int[] tablica) {
        if (tablica.length == 1 || tablica.length == 2) {
            return true;
        }
        int licznik = 0;
        int pierwszy = tablica[0];
        for (int i = 1; i < tablica.length; i++) {
            if (pierwszy >= tablica[i]) {
                licznik++;

                if (i > 1 && i < tablica.length - 1 && tablica[i - 2] >= tablica[i] && pierwszy >= tablica[i + 1]) {
                    licznik++;                                          //można też dać tutaj return false;
                } else if (i > 1 && i < tablica.length - 1 && pierwszy < tablica[i + 1]) {
                    i++;
                }
            }
            pierwszy = tablica[i];
        }
        return licznik < 2 ? true : false;
    }
}

